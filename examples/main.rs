use rand::{distributions::Standard, rngs::SmallRng, Rng, SeedableRng};
use std::{collections::HashMap, ops::Range, path::PathBuf, time};

use gannet::{
    multi_channel, multi_channel::{Channel, MultiChannelOperation}, operation::RefreshStaleOperation, uni_channel::direct, uni_channel::scatter_gather, BusDataWidth
};

fn exercise_direct_dma() {
    println!("Running direct DMA example...");

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        direct::Device::new(&s2mm_device_path, BusDataWidth::FourBytes).unwrap();
    let mut mm2s_device =
        direct::Device::new(&mm2s_device_path, BusDataWidth::FourBytes).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let s2mm_cma_size = s2mm_device.memory_pool_size();
    let mm2s_cma_size = mm2s_device.memory_pool_size();

    let mut rng = SmallRng::from_entropy();

    // Generate valid random values for the size and source & destination
    // offsets of the transfer.
    let nbytes: usize = 8 * rng.gen_range(1..(1 << 23) / 8);
    let src_offset: usize = 8 * rng.gen_range(0..(mm2s_cma_size - nbytes) / 8);
    let dest_offset: usize = 8 * rng.gen_range(0..(s2mm_cma_size - nbytes) / 8);

    println!("Setting up {:?} bytes of random data", nbytes);

    // Generate random data
    let src: Vec<u8> = (&mut rng).sample_iter(&Standard).take(nbytes).collect();
    // Create a vector into which we can copy the data out of the destination
    // memory
    let mut dest = vec![1u8; nbytes];
    // Create a vector for a sanity check on the destination memory
    let mut test = vec![0u8; nbytes];

    {
        let mut s2mm_cma = s2mm_device.get_memory(dest_offset, nbytes).unwrap();
        let mut mm2s_cma = mm2s_device.get_memory(src_offset, nbytes).unwrap();

        s2mm_cma.copy_from_slice(dest.as_slice());
        mm2s_cma.copy_from_slice(src.as_slice());

        test.copy_from_slice(s2mm_cma.as_slice());
    }

    // Sanity check to make sure the destination memory is known before the
    // transfer. This is so we know any change is cause by the DMA transfer.
    assert!(test == vec![1u8; nbytes]);

    println!("Doing transfer of {:?} bytes", nbytes);
    let start = time::Instant::now();
    // Trigger the DMA transfers
    let running_mm2s_device = mm2s_device.do_dma(src_offset, nbytes).unwrap();
    let running_s2mm_device = s2mm_device.do_dma(dest_offset, nbytes).unwrap();
    println!("do_dma took {:?}", start.elapsed());

    let timeout = time::Duration::from_secs(1);

    // Wait for DMA transfers to complete
    let (mm2s_device, _) = running_mm2s_device
        .await_completed(timeout.clone())
        .unwrap();
    let (s2mm_device, s2mm_cma) = running_s2mm_device
        .await_completed(timeout.clone())
        .unwrap();

    dest.copy_from_slice(s2mm_cma.as_slice());

    mm2s_device.print_status();
    s2mm_device.print_status();

    // Check that we have received the correct data.
    assert!(src == dest);

    println!("direct example completed");
    println!("");
}

fn exercise_sg_dma() {
    println!("Running scatter-gather example...");

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        scatter_gather::Device::new(&s2mm_device_path, BusDataWidth::FourBytes).unwrap();
    let mut mm2s_device =
        scatter_gather::Device::new(&mm2s_device_path, BusDataWidth::FourBytes).unwrap();

    let nbytes = 2560;
    let src_offset = 0;
    let dest_offset = 0;

    let mut rng = SmallRng::from_entropy();

    // Create a vector to overwrite the destination before beginning the
    // transfer
    let destination_init_data = vec![1u8; nbytes];
    // Generate random data
    let test_data: Vec<u8> = (&mut rng).sample_iter(&Standard).take(nbytes).collect();
    // Create a vector into which we can copy the data out of the destination
    // memory
    let mut destination_data = vec![0u8; nbytes];

    {
        let mut mm2s_dma_data = mm2s_device.get_memory(src_offset, nbytes).unwrap();
        let mut s2mm_dma_data = s2mm_device.get_memory(dest_offset, nbytes).unwrap();

        mm2s_dma_data.copy_from_slice(&test_data);
        s2mm_dma_data.copy_from_slice(&destination_init_data);
    }

    // Find out how many descriptors are left on the device
    let s2mm_n_avail_descriptors = s2mm_device.n_available_descriptors().unwrap();
    let mm2s_n_avail_descriptors = mm2s_device.n_available_descriptors().unwrap();

    let n_blocks = 160;
    let block_size = nbytes / n_blocks;
    let block_stride = block_size;

    // Check that we can request the number of blocks specified
    assert!(s2mm_n_avail_descriptors >= n_blocks);
    assert!(mm2s_n_avail_descriptors >= n_blocks);

    let mm2s_operation = mm2s_device
        .new_regular_blocks_operation(block_size, block_stride, n_blocks, src_offset)
        .unwrap();
    let s2mm_operation = s2mm_device
        .new_regular_blocks_operation(block_size, block_stride, n_blocks, dest_offset)
        .unwrap();

    let s2mm_running_operation = s2mm_device.do_dma(s2mm_operation).unwrap();
    let mm2s_running_operation = mm2s_device.do_dma(mm2s_operation).unwrap();

    let timeout = time::Duration::from_millis(1000);

    // Wait for DMA transfers to complete
    let (_mm2s_device, stale_mm2s_operation, _) = mm2s_running_operation
        .await_completed(timeout.clone())
        .unwrap();
    let (_s2mm_device, stale_s2mm_operation, s2mm_mem) = s2mm_running_operation
        .await_completed(timeout.clone())
        .unwrap();

    let _mm2s_operation = stale_mm2s_operation.check_and_refresh().unwrap();
    let _s2mm_operation = stale_s2mm_operation.check_and_refresh().unwrap();

    destination_data.copy_from_slice(&s2mm_mem);

    assert!(destination_data == test_data);

    println!("scatter-gather example completed");
    println!("");
}

#[allow(dead_code)]
fn exercise_mc_dma() {
    println!("Running multi-channel example...");

    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        multi_channel::Device::new(&s2mm_device_path, BusDataWidth::FourBytes).unwrap();
    let mut mm2s_device =
        multi_channel::Device::new(&mm2s_device_path, BusDataWidth::FourBytes).unwrap();

    let nbytes = 128;
    let n_channels = 16;
    let src_offset = 0;
    let dest_offset = 0;

    let mut rng = SmallRng::from_entropy();

    // Create a vector to overwrite the destination before beginning the
    // transfer
    let destination_init_data = vec![1u8; nbytes * n_channels];
    // Generate random data
    let test_data: Vec<u8> = (&mut rng)
        .sample_iter(&Standard)
        .take(nbytes * n_channels)
        .collect();
    // Create a vector into which we can copy the data out of the destination
    // memory
    let destination_data = vec![0u8; nbytes * n_channels];

    {
        let mut mm2s_mem = mm2s_device.get_memory(src_offset, test_data.len()).unwrap();
        mm2s_mem.copy_from_slice(&test_data);

        let mut s2mm_mem = s2mm_device
            .get_memory(dest_offset, destination_init_data.len())
            .unwrap();
        s2mm_mem.copy_from_slice(&destination_init_data);
    }

    // Find out how many descriptors are left on the device
    let s2mm_n_avail_descriptors = s2mm_device.n_available_descriptors().unwrap();
    let mm2s_n_avail_descriptors = mm2s_device.n_available_descriptors().unwrap();

    let n_blocks = 4;
    let block_size = nbytes / n_blocks;
    let block_stride = block_size;

    // Check that we can request the number of blocks specified
    assert!(s2mm_n_avail_descriptors >= n_blocks);
    assert!(mm2s_n_avail_descriptors >= n_blocks);

    let mut all_mm2s_operations = vec![];
    let mut all_s2mm_operations = vec![];

    println!("Setting up operations...");
    for n in 0..n_channels {
        println!(
            "Channel: {:?}, blocks: {:?}, block size: {:?}",
            n, n_blocks, block_size
        );

        let operation_src_offset = src_offset + n * nbytes;
        let operation_dest_offset = dest_offset + n * nbytes;

        let mm2s_operation = mm2s_device
            .new_regular_blocks_operation(block_size, block_stride, n_blocks, operation_src_offset)
            .unwrap();
        let s2mm_operation = s2mm_device
            .new_regular_blocks_operation(block_size, block_stride, n_blocks, operation_dest_offset)
            .unwrap();

        all_s2mm_operations.push(s2mm_operation);
        all_mm2s_operations.push(mm2s_operation);
    }

    // Create the operations hashmaps
    let mut mm2s_operations = HashMap::new();
    let mut s2mm_operations = HashMap::new();

    for (n, s2mm_operation) in all_s2mm_operations.drain(0..).enumerate() {
        s2mm_operations.insert(Channel::new(n as u8).unwrap(), s2mm_operation);
    }

    for (n, mm2s_operation) in all_mm2s_operations.drain(0..).enumerate() {
        mm2s_operations.insert(Channel::new(n as u8).unwrap(), mm2s_operation);
    }

    let s2mm_operations = MultiChannelOperation::new(s2mm_operations).unwrap();
    let mm2s_operations = MultiChannelOperation::new(mm2s_operations).unwrap();

    //    let timeout = time::Duration::from_millis(1000);
    //    let start = time::Instant::now();
    //    for _ in 0..300 {
    //        s2mm_device.do_dma(&s2mm_operations).unwrap();
    //        mm2s_device.do_dma(&mm2s_operations).unwrap();
    //        for s2mm_operation in all_s2mm_operations {
    //            // Loop over the s2mm operations, check and refresh them
    //            let s2mm_nbytes_transferred =
    //                s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();
    //            assert!(s2mm_nbytes_transferred == nbytes);
    //        }
    //
    //        for mm2s_operation in all_mm2s_operations {
    //            // Loop over the s2mm operations, check and refresh them
    //            let mm2s_nbytes_transferred =
    //                mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
    //            assert!(mm2s_nbytes_transferred == nbytes);
    //        }
    //    }
    //    println!("do_dma took {:?}", start.elapsed()/30000);

    let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();
    let mm2s_running_operation = mm2s_device.do_dma(mm2s_operations).unwrap();

    let timeout = time::Duration::from_millis(1000);

    // Wait for DMA transfers to complete
    let (_ret_mm2s_device, stale_mm2s_operations, _) = mm2s_running_operation
        .await_completed(timeout.clone())
        .unwrap();
    let (_ret_s2mm_device, stale_s2mm_operations, s2mm_mem) = s2mm_running_operation
        .await_completed(timeout.clone())
        .unwrap();

    let _mm2s_operations = stale_mm2s_operations.check_and_refresh().unwrap();
    let s2mm_operations = stale_s2mm_operations.check_and_refresh().unwrap();

    //    for (_, s2mm_operation) in s2mm_operations.channel_operations_mut() {
    //        // Loop over the s2mm operations, check and refresh them
    //        let s2mm_nbytes_transferred =
    //            s2mm_device.check_and_refresh_operation(s2mm_operation).unwrap();
    //        assert!(s2mm_nbytes_transferred == nbytes);
    //    }
    //
    //    for (_, mm2s_operation) in mm2s_operations.channel_operations_mut() {
    //        // Loop over the s2mm operations, check and refresh them
    //        let mm2s_nbytes_transferred =
    //            mm2s_device.check_and_refresh_operation(mm2s_operation).unwrap();
    //        assert!(mm2s_nbytes_transferred == nbytes);
    //    }

    let destination_slicer: Range<usize> = s2mm_operations.containing_memory().into();

    assert_eq!(
        *s2mm_mem,
        *destination_data.get(destination_slicer).unwrap()
    );

    println!("multi-channel example completed");
    println!("");
}

//fn exercise_mc_pl_generator() {
//    // This function requires:
//    // Jackdaw/jackdaw/test_utils/axis_multi_channel_packet_generator
//
//    let generator_device_path: PathBuf = ["/dev", "generator_cntrl"].iter().collect();
//
//    let file = OpenOptions::new()
//        .read(true)
//        .write(true)
//        .open(&generator_device_path).unwrap();
//
//    // Memory map the configuration register
//    let gen_mem = unsafe {
//        MmapOptions::new()
//            .offset(0)
//            .len(4096 as usize)
//            .map_mut(&file).unwrap()
//    };
//
//    // Get a raw pointer to the configuration memory
//    let gen = gen_mem.as_ptr() as *mut u32;
//
//    let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
//
//    // Create the device.
//    let mut s2mm_device = multi_channel::Device::new(&s2mm_device_path).unwrap();
//
//    let (s2mm_mem, _s2mm_mem_size) = s2mm_device.get_memory();
//
//    let n_channels = 16;
//
//    let n_blocks = 4;
//    let block_size = 32;
//    let block_stride = block_size;
//
//    let nbytes = block_size*n_blocks;
//    let dest_offset = 0;
//
//    // Create a vector to overwrite the destination before beginning the
//    // transfer
//    let destination_init_data = vec!(1u8; nbytes*n_channels);
//    // Create a vector into which we can copy the data out of the destination
//    // memory
//    let mut destination_data = vec!(0u8; nbytes*n_channels);
//
//    unsafe {
//        // Set up the destination memory with known values so we know the
//        // state of the memory before the transfer
//        copy_nonoverlapping(
//            destination_init_data.as_ptr(),
//            s2mm_mem.offset(dest_offset as isize),
//            destination_init_data.len());
//    }
//
//    // Find out how many descriptors are left on the device
//    let s2mm_n_avail_descriptors =
//        s2mm_device.n_available_descriptors().unwrap();
//
//    // Check that we can request the number of blocks specified
//    assert!(s2mm_n_avail_descriptors >= n_blocks);
//
//    let mut all_s2mm_operations = vec!();
//
//    for n in 0..n_channels {
//
//        let operation_dest_offset = dest_offset + n*block_stride*n_blocks;
//
//        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
//            block_size, block_stride, n_blocks,
//            operation_dest_offset).unwrap();
//
//        all_s2mm_operations.push(s2mm_operation);
//    }
//
//    // Create the operations hashmaps
//    let mut s2mm_operations = HashMap::new();
//
//    for (n, s2mm_operation) in all_s2mm_operations.iter().enumerate() {
//
//        s2mm_operations.insert(n as u8, s2mm_operation);
//    }
//
//    s2mm_device.do_dma(&s2mm_operations).unwrap();
//
//    // Pause to let the sytem set up all of the s2mm operations
//    let pause = time::Duration::from_secs(1);
//    thread::sleep(pause);
//
//    let timeout = time::Duration::from_secs(5);
//
//    // Need the memory fences here to prevent the cpu from reordering
//    // the non volatile reads/writes around volatile reads/writes
//    fence(Ordering::SeqCst);
//
//    // Write to the register to trigger data generation
//    unsafe {write_volatile(gen.offset(0), 1u32)};
//
//    fence(Ordering::SeqCst);
//
//    // Wait for DMA transfers to complete
//    s2mm_device.wait_transfer_complete(&timeout).unwrap();
//
//    for s2mm_operation in all_s2mm_operations {
//        // Loop over the s2mm operations, check and refresh them
//        let s2mm_nbytes_transferred =
//            s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();
//
//        assert!(s2mm_nbytes_transferred == nbytes);
//    }
//
//    unsafe {
//        // Read the destination memory.
//        copy_nonoverlapping(
//            s2mm_mem.offset(dest_offset as isize),
//            destination_data.as_mut_ptr(), nbytes*n_channels);
//    }
//
//    let mut expected_data = Vec::new();
//
//    for n in 0..n_channels as u8 {
//        for x in 0..n_blocks as u8 {
//            let channel_expected_data = vec![
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,
//                n + 1 + x*n_channels as u8, 0, 0, 0,];
//            expected_data.extend(channel_expected_data);
//        }
//    }
//
//    println!("##########################");
//    println!("{:?}", &destination_data);
//    println!("##########################");
//    println!("{:?}", &expected_data);
//
//    assert!(destination_data == expected_data);
//
//}

fn main() {
    {
        exercise_direct_dma();
    }
    {
        exercise_sg_dma();
    }
    // FIXME the exercise_mc_dma example needs fixing to actually do the DMA
    //{
    //    exercise_mc_dma();
    //}
    //    {
    //        exercise_mc_pl_generator();
    //    }
}
