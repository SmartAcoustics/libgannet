use std::fmt;
use std::mem::size_of;
use std::sync::atomic::{fence, Ordering};
use thiserror::Error;

use crate::descriptors::{Descriptor, DescriptorBlock, DescriptorError, DescriptorPool};
use crate::device_core::DeviceSpecifics;
use crate::errors::DmaCoreError;
use crate::operation::Operation;
use crate::{BusDataWidth, MemBlockLayout};

/// Error in handling scatter gather operations.
#[derive(Error, Debug)]
pub enum ScatterGatherError {
    /// Device is not halted. This operation can only run on a halted device
    #[error("Device is not halted. This operation can only run on a halted device")]
    DeviceNotHalted,
    /// Invalid size requested
    #[error("Invalid size requested")]
    InvalidSize,
    /// Invalid offset for the transfer
    #[error("Invalid offset for the transfer")]
    InvalidOffset,
    /// Invalid stride for the transfer
    #[error("Invalid stride for the transfer")]
    InvalidStride,
    /// Invalid number of blocks requested
    #[error("Invalid number of blocks requested")]
    InvalidNBlocks,
    /// The combination of nbytes and offset would cause a memory overflow
    #[error("The combination of nbytes and offset would cause a memory overflow")]
    MemoryOverflow,
    //   /// Operation error.
    //   #[error("Operation error: {source}")]
    //   Operation{#[from] source: OperationError },
    /// Descriptor handling error.
    #[error("Descriptor error: {source}")]
    Descriptor {
        #[from]
        source: DescriptorError,
    },
    /// DMA core error
    #[error("DMA core error: {source}")]
    DmaCore {
        #[from]
        source: DmaCoreError,
    },
}

pub(crate) struct ScatterGatherDeviceConfig {
    pub(crate) dma_data_size: usize,
    pub(crate) dma_data_phys_addr: usize,
    pub(crate) max_bytes_transfer_per_descriptor: usize,
    pub(crate) start_of_frame_bitmask: u32,
    pub(crate) end_of_frame_bitmask: u32,
    pub(crate) desc_field_nxt_desc_lsw_offset: usize,
    pub(crate) desc_field_nxt_desc_msw_offset: usize,
    pub(crate) desc_field_buffer_addr_lsw_offset: usize,
    pub(crate) desc_field_buffer_addr_msw_offset: usize,
    pub(crate) desc_field_control_offset: usize,
}

pub(crate) struct DescriptorFunctions {
    pub(crate) descriptor_status: fn(descriptor: &Descriptor) -> Result<usize, DmaCoreError>,
    pub(crate) refresh_descriptor: fn(descriptor: &mut Descriptor),
}

impl fmt::Debug for DescriptorFunctions {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("DescriptorFunctions {...}")
    }
}

impl Copy for DescriptorFunctions {}

impl Clone for DescriptorFunctions {
    fn clone(&self) -> Self {
        *self
    }
}

pub(crate) trait ScatterGatherDevice {
    /// Borrows the descriptor pool from this device.
    fn borrow_descriptor_pool(&mut self) -> &mut DescriptorPool;
}

pub(crate) trait ScatterGatherConfig {
    /// Returns the device configuration associated with a scatter-gather operation.
    fn scatter_gather_config(&self) -> ScatterGatherDeviceConfig;
}

/// This function writes the regular blocks descriptor chain.
///
/// To start with it checks that the arguments are valid such that:
/// * 8 < block size < max_bytes_per_descriptor
/// * block_size is a multiple of 8
///
/// * offset < dma_data_size
/// * offset is a multiple of 8
///
/// * block_size < block_stride < dma_data_size
/// * block_stride is a multiple of 8
///
/// * n_blocks can fit within the memory
///
/// * Combination of block_size, block_stride, n_blocks and offset will not
/// cause a memory overflow.
///
/// It then writes the descriptors which will cause the DMA engine to perform
/// the requested regular blocks operation.
///
/// It returns the handle to the descriptors that represents the regular
/// blocks DMA, and the number of bytes transfer the operation represents.
///
/// This function creates one descriptor per block. For MM2S transfers every
/// block will be output as a packet. For S2MM transfers the user should make
/// sure n_blocks is equal to the number of AXIS packets which will be received
/// by the DMA engine. Also in the S2MM case, the user should make sure that
/// block_size is greater than or equal to the size of the largest AXIS packet.
pub(crate) fn write_regular_blocks_descriptor_chain(
    device: &mut (impl DeviceSpecifics + ScatterGatherConfig + ScatterGatherDevice),
    block_size: usize,
    block_stride: usize,
    n_blocks: usize,
    offset: usize,
    bus_width: BusDataWidth,
) -> Result<(DescriptorBlock, usize, MemBlockLayout), ScatterGatherError> {
    // FIXME The MemBlockLayout returned from this function is a tight
    // bounding of the memory that is used. There are various potential
    // situations when this might be the different to what is wanted:
    //  1. If we have a series of regular sized blocks of which the DMA data
    //  is only a portion - e.g. IP packets where the header is set separately
    //  from the DMA.
    //  In this case we might want to extend both lower (for a non-DMA
    //  header) or higher (for the non-DMA footer of the final packet).
    //  2. If the DMA data is part of a bigger block, then again it might be
    //  necessary to extend lower and higher to encapsulate that bigger block.
    //
    // However, we don't really want to complicate the API for this block to
    // include additional facilities to extend the memory block and nor do
    // we want to always extend based on the current API (such as additional
    // trailing bytes based on the stride) as this might be the wrong thing to
    // do as well.
    //
    // Since the actual memory is
    // checked out through an Operation, it seems that is the best place to
    // put such an API change. e.g. The Operation could have an `extend_memory`
    // function or something. This would make it a more user configurable
    // concept without prejudicing what is actually required. It also means
    // not having to anything for the time being...
    //

    let sg_config = device.scatter_gather_config();

    let bus_width = bus_width as usize;

    // Check that the block size is valid. It must be a multiple of 8 and
    // in the range 8 -> max_bytes_transfer_per_descriptor. This means
    // the block size cannot be bigger than a descriptors worth of data.
    if block_size < bus_width
        || block_size > sg_config.max_bytes_transfer_per_descriptor
        || block_size % bus_width != 0
    {
        return Err(ScatterGatherError::InvalidSize);
    }

    // Check that the offset is valid. Offset cannot be less than 0 due to
    // type limits.
    if offset >= sg_config.dma_data_size || offset % bus_width != 0 {
        return Err(ScatterGatherError::InvalidOffset);
    }

    // Check that block_stride is greater than block_size and a multiple
    // of 8 and less than dma_data_size
    if block_stride < block_size ||
        block_stride >= sg_config.dma_data_size ||
            block_stride % bus_width != 0
    {
        return Err(ScatterGatherError::InvalidStride);
    }

    // Check that a valid number of n_blocks have been requested
    let max_n_blocks = sg_config.dma_data_size / block_stride;
    if n_blocks == 0 || n_blocks > max_n_blocks {
        return Err(ScatterGatherError::InvalidNBlocks);
    }

    // Check that the combination of offset, stride, n blocks, block size
    // won't cause an overflow
    //
    // offset + block_stride*(n_blocks - 1) Gives the offset of the last
    // block.
    //
    // We then add block_size - 1 to get the upper_address (need to
    // subtract 1 as offset counts from 0)
    let upper_address = offset + block_stride * (n_blocks - 1 as usize) + (block_size - 1);
    if upper_address >= sg_config.dma_data_size {
        return Err(ScatterGatherError::MemoryOverflow);
    }

    // We have specified that we can only have one descriptor per block
    let n_descriptors = n_blocks;

    const U32_SIZE: usize = size_of::<u32>();

    let mut descriptor_block = device
        .borrow_descriptor_pool()
        .request_descriptors(n_descriptors)?;

    for descriptor_num in 0..n_descriptors {
        let next_descriptor_phys_addr = {
            // Select the next descriptor in the chain. In the case of us
            // having the last descriptor, we want to select the _first_
            // descriptor in the chain. Selecting the address of the first
            // descriptor is how the DMA engine is notified the chain is
            // complete.
            let next_descriptor = &descriptor_block[(descriptor_num + 1) % n_descriptors];

            // Calculate the physical address of the next descriptor.
            next_descriptor.physical_address() as u64
        };

        let descriptor = &mut descriptor_block[descriptor_num];
        let descriptor_data = descriptor.as_mut_slice();

        // Calculate the physical address of the DMA data destination
        // (i.e. where the data gets put for this descriptor)
        let buffer_phys_addr: u64 =
            (sg_config.dma_data_phys_addr + offset + descriptor_num * block_stride) as u64;

        // Create the control word
        // In this case, we have one axi-frame per block.
        let control: u32 =
            (block_size as u32) | sg_config.start_of_frame_bitmask | sg_config.end_of_frame_bitmask;

        // Zero all the values in the descriptor
        for n in 0..descriptor_data.len() {
            descriptor_data[n] = 0
        }

        // Populate the descriptor with the relevant fields. Note: We need
        // to divide the offsets by 4 as they are given as byte offsets
        // but we are dealing in 32 bit words here.
        descriptor_data[sg_config.desc_field_nxt_desc_lsw_offset / U32_SIZE] =
            next_descriptor_phys_addr as u32;
        descriptor_data[sg_config.desc_field_nxt_desc_msw_offset / U32_SIZE] =
            (next_descriptor_phys_addr >> 32) as u32;
        descriptor_data[sg_config.desc_field_buffer_addr_lsw_offset / U32_SIZE] =
            buffer_phys_addr as u32;
        descriptor_data[sg_config.desc_field_buffer_addr_msw_offset / U32_SIZE] =
            (buffer_phys_addr >> 32) as u32;
        descriptor_data[sg_config.desc_field_control_offset / U32_SIZE] = control;
    }

    // We add a fence to make sure all the descriptors are written before
    // anything else can happen.
    fence(Ordering::SeqCst);

    let transfer_bytes = n_blocks * block_size;

    let containing_size = (n_blocks - 1) * block_stride + block_size;
    let containing_memory = MemBlockLayout {
        offset,
        size: containing_size,
    };

    Ok((descriptor_block, transfer_bytes, containing_memory))
}

/// A function to walk the descriptor chain. Checking the status of the
/// descriptors, summing the number of bytes transferred and refreshing the
/// descriptors. If the descriptor is reporting an error, this function will
/// return that error. If none of the descriptors are reporting an error then
/// this function will return the number of bytes transferred. This function
/// will always refresh all descriptors in the chain even if it encounters an
/// error.
pub(crate) fn walk_descriptor_chain_check_and_refresh(
    operation: &mut Operation,
) -> Result<usize, DmaCoreError> {
    // We need to make sure all other read/writes prior to this one have
    // completed satisfactorily, and make sure the changes happen _after_
    // that. SeqCst is the only ordering the guarantees that.
    fence(Ordering::SeqCst);

    let DescriptorFunctions {
        descriptor_status,
        refresh_descriptor,
    } = operation.descriptor_functions();

    let mut error: Option<DmaCoreError> = None;

    // Get the number of descriptors from the operation
    let descriptor_block = operation.descriptor_block_mut();

    let mut nbytes: usize = 0;

    // Iterate over the descriptors
    for descriptor in descriptor_block.iter_mut() {
        if error.is_none() {
            // If we have not yet encountered an error, check the
            // descriptor status. If an error is encountered on this
            // descriptor, store it to return. Otherwise sum the nbytes
            // transferred. Note we only return the first error we
            // encounter in the decriptor chain. Also note that we don't
            // just return the error immediately here because we want to
            // finish refreshing the descriptor chain even if it has
            // errored
            match descriptor_status(descriptor) {
                Err(desc_err) => error = Some(desc_err),
                Ok(desc_nbytes) => nbytes += desc_nbytes,
            }
        }

        // Overwrite the status register to clear the complete bit
        refresh_descriptor(descriptor);
    }

    // We add a fence to make sure all the descriptors are written before
    // anything else can happen.
    fence(Ordering::SeqCst);

    if error.is_none() {
        // No error was encountered so return the number of bytes
        // transferred
        Ok(nbytes)
    } else {
        // Error was encountered so return it
        Err(error.unwrap())
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod tests {
    use std::path::PathBuf;
    use crate::BusDataWidth;

    use crate::uni_channel::scatter_gather;

    /// `write_regular_blocks_descriptor_chain` should return as the third
    /// entry in the tuple the bounding memory layout of the operation.
    #[test]
    fn test_write_regular_blocks_descriptor_chain_layout() {
        let setups = vec![
            (1024, 1024, 1, 0, 1024),
            (1024, 1024, 2, 0, 2048),
            (1024, 1024, 10, 200, 10240),
            (1024, 1032, 1, 0, 1024),
            (1024, 1032, 2, 0, 2056),
            (1024, 1032, 10, 0, 10312),
            (512, 1024, 4, 200, 3584),
            (1024, 1120, 25, 96, 27904),
        ];

        // Create device
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = scatter_gather::Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        for (block_size, block_stride, n_blocks, offset, expected_size) in setups {
            let operation = device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
                .unwrap();

            let containing_memory = operation.containing_memory();

            assert_eq!(containing_memory.offset, offset);
            assert_eq!(containing_memory.size, expected_size);
        }
    }
}
