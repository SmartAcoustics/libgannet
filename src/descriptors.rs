use std::fmt;
use std::mem::size_of;
use std::ops::{Index, IndexMut};
use std::slice;
use std::sync::atomic::{AtomicBool, AtomicU64, Ordering};
use std::sync::Arc;

use memmap2::MmapMut;
use thiserror::Error;

static NEXT_POOL_ID: AtomicU64 = AtomicU64::new(0);

/// The descriptor alignment is required by the IP block
/// (note this alignment is in words, not bytes)
/// (see next descriptor pointer fields documentation in pg288 and pg021)
pub(crate) const DESCRIPTOR_ALIGNMENT: usize = 16;

pub(crate) const DESCRIPTOR_SIZE: usize = 13; // u32 words
pub(crate) const DESCRIPTOR_STRIDE: usize = DESCRIPTOR_ALIGNMENT;

pub(crate) const DESCRIPTOR_SIZE_BYTES: usize = DESCRIPTOR_SIZE * size_of::<u32>();
pub(crate) const DESCRIPTOR_STRIDE_BYTES: usize = DESCRIPTOR_STRIDE * size_of::<u32>();

/// Errors in handling the scatter-gather descriptors.
#[derive(Error, Debug, PartialEq)]
pub enum DescriptorError {
    /// A requested descriptor block is unknown to the pool.
    #[error("The requested descriptor block is unknown to this pool.")]
    UnknownDescriptorBlock,
    /// Insufficient descriptors available to satisfy a request. This could be
    /// due to the descriptor memory space being insufficiently large for the
    /// task at hand, or that too many operations have been created and are
    /// still in scope.
    #[error("Not enough descriptors available to satisfy the request.")]
    InsufficientDescriptors,
}

#[derive(Debug, Clone)]
struct DescriptorConfig {
    offset: usize,
    physical_base_address: usize,
}

#[derive(Debug)]
pub(crate) struct Descriptor {
    inner_ptr: *mut u32,
    physical_address: usize,
}

impl Descriptor {
    /// Returns a descriptor, representing a block of memory at the given
    /// descriptor offset. The offset is the index of the descriptor. So
    /// the first descriptor is `offset` zero, the second is `offset` one etc.
    /// It is _not_ the offset in bytes.
    ///
    /// `phys_base_addr` is the _physical_ base address of the descriptor
    /// memory space.
    ///
    /// This is unsafe because:
    ///
    /// 1. It is up to the calling code to make sure
    /// a descriptor does not alias with another descriptor (i.e. with the
    /// same `offset` and `mem`). Only a single descriptor can exist with
    /// a given `offset` and `mem`.
    /// 2. The underlying memory must exist as long as the descriptor might be
    /// accessed. This is currently done by enforcing all Descriptors to belong
    /// to a DescriptorBlock that also owns an Arc<MmapMut>. If the Descriptor
    /// is ever needed to be owned itself, it would be necessary for it to have
    /// its own Arc<MmapMut>.
    unsafe fn new(mem: &Arc<MmapMut>, config: &DescriptorConfig) -> Descriptor {
        let DescriptorConfig {
            offset,
            physical_base_address,
        } = config;

        let word_offset = offset * DESCRIPTOR_STRIDE;
        let bytes_offset = offset * DESCRIPTOR_STRIDE_BYTES;

        // Make sure all descriptor access are within the memory map.
        if bytes_offset + DESCRIPTOR_SIZE_BYTES > mem.len() {
            panic!("The data access must fit within the memory map.");
        }

        let physical_address = physical_base_address + bytes_offset;

        // We add the byte offset to get the descriptor start
        let ptr = (mem.as_ptr() as *mut u32).add(word_offset);
        Descriptor {
            inner_ptr: ptr,
            physical_address,
        }
    }

    pub(crate) fn physical_address(&self) -> usize {
        self.physical_address
    }

    pub(crate) fn as_slice(&self) -> &[u32] {
        unsafe { slice::from_raw_parts(self.inner_ptr, DESCRIPTOR_SIZE) }
    }

    pub(crate) fn as_mut_slice(&mut self) -> &mut [u32] {
        unsafe { slice::from_raw_parts_mut(self.inner_ptr, DESCRIPTOR_SIZE) }
    }
}

// As long as the invariants on creating and using a Descriptor are enforced,
// it will be Send. Indeed, the whole point is to create a chunk of memory
// that is Send.
unsafe impl Send for Descriptor {}

// It's also Sync because there is no way to mutate its interior without having
// a mutable Descriptor.
unsafe impl Sync for Descriptor {}

//#[derive(Debug)]
//pub(crate) struct DescriptorBlockHandle {
//    descriptor_block_id: u64,
//    handle_dropped: Arc<AtomicBool>,
//    pool_id: u64,
//}
//
//impl Drop for DescriptorBlockHandle {
//
//    fn drop(&mut self)  {
//        self.handle_dropped.store(true, Ordering::Release);
//    }
//}

#[derive(Debug)]
pub(crate) struct DescriptorBlock {
    descriptors: Vec<Descriptor>,
    dropped: Arc<AtomicBool>,
    pool_id: u64,
    /// A reference to the Mmap is kept as part of the descriptor block. As long
    /// as the descriptor block is in scope, this prevents the memory the descriptors
    /// refer to being deallocated. This is in last position so it is dropped last,
    /// though this is probably not super important.
    _mem: Arc<MmapMut>,
}

impl DescriptorBlock {
    pub(crate) fn len(&self) -> usize {
        self.descriptors.len()
    }

    pub(crate) fn iter_mut(&mut self) -> slice::IterMut<'_, Descriptor> {
        self.descriptors.iter_mut()
    }

    pub(crate) fn pool_id(&self) -> u64 {
        self.pool_id
    }
}

impl Drop for DescriptorBlock {
    fn drop(&mut self) {
        self.dropped.store(true, Ordering::Release);
    }
}

impl Index<usize> for DescriptorBlock {
    type Output = Descriptor;

    fn index(&self, idx: usize) -> &Descriptor {
        &self.descriptors[idx]
    }
}

impl IndexMut<usize> for DescriptorBlock {
    fn index_mut(&mut self, idx: usize) -> &mut Descriptor {
        &mut self.descriptors[idx]
    }
}

#[derive(Debug)]
struct AllocatedDescriptorBlock {
    descriptor_configs: Vec<DescriptorConfig>,
    descriptor_block_dropped: Arc<AtomicBool>,
}

impl AllocatedDescriptorBlock {
    pub(crate) fn len(&self) -> usize {
        self.descriptor_configs.len()
    }
}

/// A pool of descriptor memory.
pub struct DescriptorPool {
    mem: Arc<MmapMut>,
    available_descriptors: Vec<DescriptorConfig>,
    allocated_descriptor_blocks: Vec<AllocatedDescriptorBlock>,
    pool_id: u64,
}

impl fmt::Debug for DescriptorPool {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("DescriptorPool")
            .field("mem", &self.mem)
            .field(
                "available_descriptors",
                &format_args!("[{:?} x Descriptor]", self.available_descriptors.len()),
            )
            .field(
                "allocated_descriptor_blocks",
                &format_args!(
                    "[{:?} x Descriptor]",
                    self.allocated_descriptor_blocks.len()
                ),
            )
            .field("pool_id", &self.pool_id)
            .finish()
    }
}

impl DescriptorPool {
    /// Unsafe because the physical_address must be set correctly for the
    /// descriptors to be set up correctly.
    pub(crate) unsafe fn new(mem: MmapMut, physical_address: usize) -> DescriptorPool {
        // We firstly make the physical address aligned to the descriptor
        // alignment, which is 16 bytes. If it's page aligned, it will already
        // be aligned so.
        // The following alignment offset works for any value of alignment
        // offset and will never overflow, at the cost of two remainder ops,
        // which are in any case simplified by the optimiser as DESCRIPTOR_ALIGN
        // is a power of two.
        const ALIGN: usize = DESCRIPTOR_ALIGNMENT;
        let alignment_offset = (ALIGN - (physical_address % ALIGN)) % ALIGN;

        let pool_id = NEXT_POOL_ID.fetch_add(1, Ordering::Relaxed);

        let physical_base_address = physical_address + alignment_offset;

        let mem = Arc::new(mem);
        let n_descriptors = (mem.len() - alignment_offset) / DESCRIPTOR_STRIDE_BYTES;

        let mut available_descriptors = Vec::with_capacity(n_descriptors);

        for n in 0..n_descriptors {
            let descriptor_config = DescriptorConfig {
                offset: n,
                physical_base_address,
            };

            available_descriptors.push(descriptor_config);
        }

        // We reverse it to have the lowest address descriptors allocated first
        // to keep things more like one might expect.
        available_descriptors.reverse();

        let allocated_descriptor_blocks = Vec::new();

        DescriptorPool {
            mem,
            available_descriptors,
            allocated_descriptor_blocks,
            pool_id,
        }
    }

    /// Returns true if the descriptor block is from this DescriptorPool.
    #[inline]
    pub(crate) fn contains(&self, block: &DescriptorBlock) -> bool {
        block.pool_id == self.pool_id
    }

    #[inline]
    pub(crate) fn pool_id(&self) -> u64 {
        self.pool_id
    }

    /// Returns the number of descriptors that are available.
    pub(crate) fn descriptors_available(&self) -> usize {
        let mut descriptors_available = self.available_descriptors.len();

        for descriptor_block in self.allocated_descriptor_blocks.iter() {
            if descriptor_block
                .descriptor_block_dropped
                .load(Ordering::Acquire)
            {
                descriptors_available += descriptor_block.len()
            }
        }

        descriptors_available
    }

    pub(crate) fn request_descriptors(
        &mut self,
        n: usize,
    ) -> Result<DescriptorBlock, DescriptorError> {
        // Remove all the descriptor blocks that have been dropped and put them
        // back into the available_descriptors pool.
        //
        // It's not obvious of the best (i.e. most efficient) way to do this,
        // but allocating a new hashmap and either moving it in or deleting
        // it seems reasonable.
        //
        // We create one extra space so we can be sure we can include our
        // new descriptor block (the +1).
        let mut updated_allocated_descriptor_blocks =
            Vec::with_capacity(self.allocated_descriptor_blocks.len() + 1);

        for descriptor_block in self.allocated_descriptor_blocks.drain(0..) {
            if descriptor_block
                .descriptor_block_dropped
                .load(Ordering::Acquire)
            {
                // We can add the allocated blocks back to the pool
                let AllocatedDescriptorBlock {
                    mut descriptor_configs,
                    descriptor_block_dropped: _,
                } = descriptor_block;

                // Copy the descriptor configs vec out of the descriptor
                // block and into the available descriptors.
                self.available_descriptors.append(&mut descriptor_configs);
            } else {
                updated_allocated_descriptor_blocks.push(descriptor_block);
            }
        }

        self.allocated_descriptor_blocks = updated_allocated_descriptor_blocks;

        // We can now try to allocate our new descriptor block
        let n_available = self.available_descriptors.len();

        if n_available < n {
            return Err(DescriptorError::InsufficientDescriptors);
        }

        // From after this point until the DescriptorBlock is created and a
        // reference to it is kept locally, there must be no errors,
        // otherwise the descriptors may be lost.
        let descriptor_configs = self.available_descriptors.split_off(n_available - n);

        let block_dropped = Arc::new(AtomicBool::new(false));

        let descriptor_block = DescriptorBlock {
            descriptors: descriptor_configs
                .iter()
                .map(|config| unsafe { Descriptor::new(&self.mem, config) })
                .collect(),
            dropped: block_dropped.clone(),
            pool_id: self.pool_id,
            _mem: self.mem.clone(),
        };

        let allocated_descriptor_block = AllocatedDescriptorBlock {
            descriptor_configs,
            descriptor_block_dropped: block_dropped,
        };

        self.allocated_descriptor_blocks
            .push(allocated_descriptor_block);
        // It's safe to have errors again (on descriptor_block dropped,
        // the block_dropped flag is set and the descriptors can be recovered).

        Ok(descriptor_block)
    }

    //    pub(crate) fn borrow_descriptors(&self, handle: &DescriptorBlockHandle)
    //        -> Result<&DescriptorBlock, DescriptorError>
    //    {
    //        if let Some(descriptor_block) = self.allocated_descriptor_blocks.get(
    //            &handle.descriptor_block_id) {
    //
    //            Ok(descriptor_block)
    //        } else {
    //            Err(DescriptorError::UnknownDescriptorBlock)
    //        }
    //    }
    //
    //    pub(crate) fn borrow_descriptors_mut(&mut self, handle: &DescriptorBlockHandle)
    //        -> Result<&mut DescriptorBlock, DescriptorError>
    //    {
    //        if let Some(descriptor_block) = self.allocated_descriptor_blocks.get_mut(
    //            &handle.descriptor_block_id) {
    //
    //            Ok(descriptor_block)
    //        } else {
    //            Err(DescriptorError::UnknownDescriptorBlock)
    //        }
    //    }
}

#[cfg(test)]
mod tests {

    use super::*;
    const SIZE: usize = 1000;

    fn make_mem() -> MmapMut {
        let mut mem = MmapMut::map_anon(SIZE).unwrap();

        // Hackily put some data in
        for n in 0..SIZE {
            mem[n] = (n % (u8::MAX as usize + 1)) as u8;
        }

        mem
    }

    #[test]
    fn test_create_and_request_descriptors() {
        let mem = make_mem();
        let mut pool = unsafe { DescriptorPool::new(mem, 0) };

        // We check out every descriptor so we can make sure non overlap
        let mut descriptor_block = pool
            .request_descriptors(pool.descriptors_available())
            .unwrap();

        for n in 0..descriptor_block.len() {
            let descriptor_mut = descriptor_block[n].as_mut_slice();

            for p in 0..descriptor_mut.len() {
                descriptor_mut[p] = n as u32;
            }
        }

        for n in 0..descriptor_block.len() {
            let descriptor = descriptor_block[n].as_slice();

            for p in 0..descriptor.len() {
                assert!(descriptor[p] == n as u32);
            }
        }
    }

    #[test]
    fn test_blocks_dropped() {
        let mem = make_mem();
        let mut pool = unsafe { DescriptorPool::new(mem, 0) };

        // We check out every descriptor so we can make sure non overlap
        {
            let _descriptor_block = pool
                .request_descriptors(pool.descriptors_available())
                .unwrap();

            let try_more_blocks = pool.request_descriptors(1);
            assert!(try_more_blocks.unwrap_err() == DescriptorError::InsufficientDescriptors);
        }

        // Now we should be able to check some out again because the handle has
        // been dropped.
        let try_more_blocks = pool.request_descriptors(1);
        assert!(try_more_blocks.is_ok());

        let try_more_blocks = pool.request_descriptors(10);
        assert!(try_more_blocks.is_ok());
    }

    #[test]
    fn test_descriptors_available() {
        let mem = make_mem();
        let mut pool = unsafe { DescriptorPool::new(mem, 0) };

        let total_descriptors = pool.descriptors_available();

        // We check out every descriptor so we can make sure non overlap
        {
            let _descriptor_block = pool.request_descriptors(total_descriptors).unwrap();

            assert!(pool.descriptors_available() == 0);
        }

        assert!(pool.descriptors_available() == total_descriptors);

        let _try_more_blocks = pool.request_descriptors(1);
        assert!(pool.descriptors_available() == total_descriptors - 1);

        let _try_more_blocks = pool.request_descriptors(10);
        assert!(pool.descriptors_available() == total_descriptors - 11);
    }

    #[test]
    /// The `DescriptorPool` should have a method `contains` that returns
    /// whether a `DescriptorBlockHandle` is from that pool.
    fn test_pool_contains_block_handle() {
        let mem1 = make_mem();
        let mem2 = make_mem();

        let mut pool1 = unsafe { DescriptorPool::new(mem1, 0) };
        let mut pool2 = unsafe { DescriptorPool::new(mem2, 0) };
        let block1 = pool1.request_descriptors(5).unwrap();
        let block2 = pool2.request_descriptors(5).unwrap();

        assert!(pool1.contains(&block1));
        assert!(pool2.contains(&block2));
        assert!(!pool1.contains(&block2));
        assert!(!pool2.contains(&block1));
    }

    #[test]
    /// The `physical_address` method of the descriptor should return a valid
    /// aligned physical address, correctly offset from the base address.
    fn test_descriptor_physical_address() {
        // Some page aligned address
        // This can be arbitrary as it only needs to be correct when used by
        // the DMA engine.
        let aligned_addr = 0xFFF000;

        for phys_offset in 0..DESCRIPTOR_ALIGNMENT {
            let mem = make_mem();
            let mut pool = unsafe { DescriptorPool::new(mem, aligned_addr - phys_offset) };

            assert!(pool.descriptors_available() == SIZE / DESCRIPTOR_STRIDE_BYTES);

            let descriptor_block = pool
                .request_descriptors(pool.descriptors_available())
                .unwrap();

            for n in 0..descriptor_block.len() {
                // Use inv_n because the lowest mem descriptors are at the end
                // of the array so are allocated first.
                let inv_n = descriptor_block.len() - 1 - n;
                let desc_phys_addr = descriptor_block[inv_n].physical_address();

                assert!(desc_phys_addr == aligned_addr + n * DESCRIPTOR_STRIDE_BYTES);
                assert!(desc_phys_addr % DESCRIPTOR_ALIGNMENT == 0);
            }
        }
    }
}
