use crate::errors::{DeviceError, DmaCoreError};
use crate::multi_channel::Device;
use crate::multi_channel::{
    Channel, Mm2sChannelStatus, MultiChannelOperation, MultiChannelOperationRefreshError,
    MultiChannelTransferError, Registers, S2mmChannelStatus, StaleMultiChannelOperation,
    MAX_CHANNELS,
};

use crate::operation::{OperationError, RefreshStaleOperation};
use crate::{
    device_core::DeviceSpecifics,
    utils::{enable_interrupts, get_and_consume_interrupts},
    MemBlock, TransferType,
};

use std::time;

#[cfg(feature = "async")]
use crate::utils::async_get_and_consume_interrupts;

#[cfg(feature = "async")]
use tokio::{io::unix::AsyncFd, time::timeout};

#[derive(Debug)]
pub struct RunningDevice {
    pub(super) device: Device,
    pub(super) operation: MultiChannelOperation,
    pub(super) mem_block: MemBlock,
}

impl RunningDevice {
    fn inner_await_completed(
        &mut self,
        timeout_duration: time::Duration,
    ) -> Result<(), DeviceError> {
        // This function performs the following tasks:
        //     Check enabled reg to see which channels should interrupt
        //     Check all enabled channels have sent an interrupt
        //     Check common error reg to check for errors
        //     Check common status reg to look for all idle
        //     Write to each channel status register to clear interrupts
        //     Set the channel enabled reg to 0
        //     Write the stop bit
        //     Wait for device halted

        // Determine which channel enable/disable register and interrupt
        // status register needs to be read depending on the transfer
        // type.
        let (ch_en_reg, interrupt_status_reg) = match self.device.transfer_type {
            TransferType::MM2S => (Registers::Mm2sChannelEnable, Registers::Mm2sInterruptStatus),
            TransferType::S2MM => (Registers::S2mmChannelEnable, Registers::S2mmInterruptStatus),
        };

        // Read the channel enable register
        let enabled_channels = self.device.raw_device.read_reg(ch_en_reg.clone());

        // Get the time now
        let pre_interrupt_time_check = time::Instant::now();

        loop {
            // Get the time left in the timeout
            let timeout_used = pre_interrupt_time_check.elapsed();
            let timeout_remaining = if timeout_used > timeout_duration {
                time::Duration::from_secs(0)
            } else {
                timeout_duration - timeout_used
            };

            // An interrupt means that the DMA has completed on at least
            // one channel. If get_and_consume_interrupts returns 0 then
            // it timedout before detecting an interrupt.
            if get_and_consume_interrupts(&mut self.device.file, &timeout_remaining)? == 0 {
                // Check for block descriptor shortfall in the case of S2MM.
                // This never happens on MM2S devices (indeed, there isn't a
                // bit defined in that case).
                match self.device.transfer_type {
                    TransferType::MM2S => (),
                    TransferType::S2MM => {
                        let bd_shortfall_bit = S2mmChannelStatus::BDShortfall.bitmask();

                        for n in 0..MAX_CHANNELS {
                            if (enabled_channels & (0b1 << n)) != 0x0 {
                                let channel = Channel::new(n as u8)
                                    .expect("Channel creation should never fail and this is a bug");

                                // Read the status register of each channel
                                let channel_status = self
                                    .device
                                    .raw_device
                                    .read_reg(Registers::S2mmChStatus(channel));

                                if channel_status & bd_shortfall_bit != 0 {
                                    return Err(DmaCoreError::BDShortfall.into());
                                }
                            }
                        }
                    }
                }

                // Poll timed out
                return Err(DeviceError::TimedOut);
            }

            // Need to enable the interrupts for any further interrupts.
            // This needs to be enabled before we read the interrupted
            // channels register to prevent a race condition after reading
            // the interrupted channels but before enabling interrupts
            enable_interrupts(&mut self.device.file)?;

            // Read which channels have interrupted
            let interrupted_channels: u32 = self
                .device
                .raw_device
                .read_reg(interrupt_status_reg.clone());

            // If all of the enabled channels have sent an interrupt then
            // move on. We need to AND the interrupted channels with the
            // enabled channels so we can handle the hypothetical
            // situation when channels which are not enabled also send an
            // interrupt
            // Otherwise, we move on and wait for another interrupt.
            if (interrupted_channels & enabled_channels) == enabled_channels {
                // Do a get_and_consume_interrupts here to clear any
                // interrupts which arrived after the previous
                // get_and_consume_interrupts but before we read the
                // interrupted channels reg
                // There should never be any more interrupts once all the
                // enabled channels have interrupted.
                get_and_consume_interrupts(&mut self.device.file, &time::Duration::from_secs(0))?;
                break;
            }
        }

        // Check the error status of the DMA transfer.
        self.device.check_error()?;

        // Work out how much of the timeout we have remaining after some
        // was used waiting for the interrupts.
        let timeout_used = pre_interrupt_time_check.elapsed();
        let timeout_remaining = if timeout_used > timeout_duration {
            time::Duration::from_secs(0)
        } else {
            timeout_duration - timeout_used
        };

        // If we are doing a transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when the data has streamed out of the DMA engine and the
        // descriptors have been updated properly.
        // When the device has returned to idle, it has definitely finished
        // all pending transactions.
        self.device.wait_for_idle(&timeout_remaining)?;

        // Write to the stop bit to halt the device
        let control_reg = match self.device.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonControl,
            TransferType::S2MM => Registers::S2mmCommonControl,
        };

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        self.device.raw_device.write_reg(control_reg, 0_u32);

        // Wait for the status register to show the device is halted. At
        // that point, we can refresh the descriptor chain.
        self.device
            .wait_for_halted(&time::Duration::from_millis(10))?;

        // Clear the interrupts
        // Clear the channel control registers to set the fetch bit low
        // and turn off the interrupts.
        let status_reg_irq_clear_bits = match self.device.transfer_type {
            TransferType::MM2S => {
                Mm2sChannelStatus::OtherChErrIrq.bitmask()
                    | Mm2sChannelStatus::IocIrq.bitmask()
                    | Mm2sChannelStatus::ErrIrq.bitmask()
            }
            TransferType::S2MM => {
                S2mmChannelStatus::OtherChErrIrq.bitmask()
                    | S2mmChannelStatus::IocIrq.bitmask()
                    | S2mmChannelStatus::ErrIrq.bitmask()
            }
        };

        match self.device.transfer_type {
            TransferType::MM2S => {
                for n in 0..MAX_CHANNELS {
                    if (enabled_channels & (0b1 << n)) != 0x0 {
                        let channel = Channel::new(n as u8)
                            .expect("Channel creation should never fail and this is a bug");

                        self.device
                            .raw_device
                            .write_reg(Registers::Mm2sChStatus(channel), status_reg_irq_clear_bits);
                        self.device
                            .raw_device
                            .write_reg(Registers::Mm2sChControl(channel), 0_u32);
                    }
                }
            }
            TransferType::S2MM => {
                for n in 0..MAX_CHANNELS {
                    if (enabled_channels & (0b1 << n)) != 0x0 {
                        let channel = Channel::new(n as u8)
                            .expect("Channel creation should never fail and this is a bug");

                        self.device
                            .raw_device
                            .write_reg(Registers::S2mmChStatus(channel), status_reg_irq_clear_bits);
                        self.device
                            .raw_device
                            .write_reg(Registers::S2mmChControl(channel), 0_u32);
                    }
                }
            }
        }

        // Disable all channels by writing 0 to the channel enable
        // register
        self.device.raw_device.write_reg(ch_en_reg, 0_u32);

        Ok(())
    }

    async fn inner_completed(
        &mut self,
        timeout_duration: time::Duration,
    ) -> Result<(), DeviceError> {
        // FIXME register reads and writes should be async

        // This function performs the following tasks:
        //     Check enabled reg to see which channels should interrupt
        //     Check all enabled channels have sent an interrupt
        //     Check common error reg to check for errors
        //     Check common status reg to look for all idle
        //     Write to each channel status register to clear interrupts
        //     Set the channel enabled reg to 0
        //     Write the stop bit
        //     Wait for device halted

        // Determine which channel enable/disable register and interrupt
        // status register needs to be read depending on the transfer
        // type.
        let (ch_en_reg, interrupt_status_reg) = match self.device.transfer_type {
            TransferType::MM2S => (Registers::Mm2sChannelEnable, Registers::Mm2sInterruptStatus),
            TransferType::S2MM => (Registers::S2mmChannelEnable, Registers::S2mmInterruptStatus),
        };

        // Read the channel enable register
        let enabled_channels = self.device.raw_device.read_reg(ch_en_reg.clone());

        // Get the time now
        let pre_interrupt_time_check = time::Instant::now();

        // We can't use `get_or_insert_with` here because the creation of
        // the AsyncFd object may error.
        if self.device.async_file.is_none() {
            self.device
                .async_file
                .replace(AsyncFd::new(self.device.file.try_clone()?)?);
        }

        let mut async_file = self
            .device
            .async_file
            .take()
            .expect("At this point, async_file should always contain something.");

        loop {
            // Get the time left in the timeout
            let timeout_used = pre_interrupt_time_check.elapsed();
            let timeout_remaining = if timeout_used > timeout_duration {
                time::Duration::from_secs(0)
            } else {
                timeout_duration - timeout_used
            };

            let _n_interrupts = match timeout(
                timeout_remaining,
                async_get_and_consume_interrupts(&mut async_file),
            )
            .await
            {
                Err(_) => {
                    // Check for block descriptor shortfall in the case of S2MM.
                    // This never happens on MM2S devices (indeed, there isn't a
                    // bit defined in that case).
                    match self.device.transfer_type {
                        TransferType::MM2S => (),
                        TransferType::S2MM => {
                            let bd_shortfall_bit = S2mmChannelStatus::BDShortfall.bitmask();

                            for n in 0..MAX_CHANNELS {
                                if (enabled_channels & (0b1 << n)) != 0x0 {
                                    let channel = Channel::new(n as u8).expect(
                                        "Channel creation should \
                                                never fail and this is a bug",
                                    );
                                    // Read the status register of each channel
                                    let channel_status = self
                                        .device
                                        .raw_device
                                        .read_reg(Registers::S2mmChStatus(channel));

                                    if channel_status & bd_shortfall_bit != 0 {
                                        return Err(DmaCoreError::BDShortfall.into());
                                    }
                                }
                            }
                        }
                    }

                    return Err(DeviceError::TimedOut);
                }
                Ok(read_result) => read_result?,
            };

            // Need to enable the interrupts for any further interrupts.
            // This needs to be enabled before we read the interrupted
            // channels register to prevent a race condition after reading
            // the interrupted channels but before enabling interrupts
            enable_interrupts(&mut self.device.file)?;

            // Read which channels have interrupted
            let interrupted_channels: u32 = self
                .device
                .raw_device
                .read_reg(interrupt_status_reg.clone());

            // If all of the enabled channels have sent an interrupt then
            // move on. We need to AND the interrupted channels with the
            // enabled channels so we can handle the hypothetical
            // situation when channels which are not enabled also send an
            // interrupt
            // Otherwise, we move on and wait for another interrupt.
            if (interrupted_channels & enabled_channels) == enabled_channels {
                // Do a synchronous get_and_consume_interrupts here to clear any
                // interrupts which arrived after the previous
                // get_and_consume_interrupts but before we read the
                // interrupted channels reg
                // There should never be any more interrupts once all the
                // enabled channels have interrupted.
                get_and_consume_interrupts(async_file.get_mut(), &time::Duration::from_secs(0))?;
                break;
            }
        }

        // Check the error status of the DMA transfer.
        self.device.check_error()?;

        // Work out how much of the timeout we have remaining after some
        // was used waiting for the interrupts.
        let timeout_used = pre_interrupt_time_check.elapsed();
        let timeout_remaining = if timeout_used > timeout_duration {
            time::Duration::from_secs(0)
        } else {
            timeout_duration - timeout_used
        };

        // If we are doing a transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when the data has streamed out of the DMA engine and the
        // descriptors have been updated properly.
        // When the device has returned to idle, it has definitely finished
        // all pending transactions.
        // FIXME make async
        self.device.wait_for_idle(&timeout_remaining)?;

        // Write to the stop bit to halt the device
        let control_reg = match self.device.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonControl,
            TransferType::S2MM => Registers::S2mmCommonControl,
        };

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        self.device.raw_device.write_reg(control_reg, 0_u32);

        // Wait for the status register to show the device is halted. At
        // that point, we can refresh the descriptor chain.
        // FIXME make async
        self.device
            .wait_for_halted(&time::Duration::from_millis(10))?;

        // Clear the interrupts
        // Clear the channel control registers to set the fetch bit low
        // and turn off the interrupts.
        let status_reg_irq_clear_bits = match self.device.transfer_type {
            TransferType::MM2S => {
                Mm2sChannelStatus::OtherChErrIrq.bitmask()
                    | Mm2sChannelStatus::IocIrq.bitmask()
                    | Mm2sChannelStatus::ErrIrq.bitmask()
            }
            TransferType::S2MM => {
                S2mmChannelStatus::OtherChErrIrq.bitmask()
                    | S2mmChannelStatus::IocIrq.bitmask()
                    | S2mmChannelStatus::ErrIrq.bitmask()
            }
        };

        match self.device.transfer_type {
            TransferType::MM2S => {
                for n in 0..MAX_CHANNELS {
                    if (enabled_channels & (0b1 << n)) != 0x0 {
                        let channel = Channel::new(n as u8)
                            .expect("Channel creation should never fail and this is a bug");

                        self.device
                            .raw_device
                            .write_reg(Registers::Mm2sChStatus(channel), status_reg_irq_clear_bits);
                        self.device
                            .raw_device
                            .write_reg(Registers::Mm2sChControl(channel), 0_u32);
                    }
                }
            }
            TransferType::S2MM => {
                for n in 0..MAX_CHANNELS {
                    if (enabled_channels & (0b1 << n)) != 0x0 {
                        let channel = Channel::new(n as u8)
                            .expect("Channel creation should never fail and this is a bug");

                        self.device
                            .raw_device
                            .write_reg(Registers::S2mmChStatus(channel), status_reg_irq_clear_bits);
                        self.device
                            .raw_device
                            .write_reg(Registers::S2mmChControl(channel), 0_u32);
                    }
                }
            }
        }

        // Disable all channels by writing 0 to the channel enable
        // register
        self.device.raw_device.write_reg(ch_en_reg, 0_u32);

        Ok(())
    }

    /// Blocking wait for completion of transfers on the running device, with
    /// a timeout.
    ///
    /// Once this method returns, the [`Device`] will be ready to use again.
    /// The associated operation is returned in the form of a
    /// [`StaleMultiChannelOperation`]. This must be refreshed to recover the
    /// useable [`MultiChannellOperation`].
    ///
    /// On error, a [`MultiChannelTransferError`] will be returned which
    /// encapsulates the error information, as well as the [`Device`] and the
    /// associated [`StaleMultiChannelOperation`].
    pub fn await_completed(
        mut self,
        timeout_duration: time::Duration,
    ) -> Result<(Device, StaleMultiChannelOperation, MemBlock), MultiChannelTransferError> {
        // Call inner_completed which borrows self mutably, so the usual ?
        // semantics work fine and we can still destructure self afterwards.
        let res = self.inner_await_completed(timeout_duration);
        let Self {
            device,
            operation,
            mem_block,
        } = self;

        match res {
            Err(err) => Err(MultiChannelTransferError {
                source: err,
                errored_device: (device, operation.into()).into(),
            }),
            Ok(()) => Ok((device, operation.into(), mem_block)),
        }
    }

    /// Async wait for completion of transfers on the running device, with
    /// a timeout.
    ///
    /// Once this method returns, the [`Device`] will be ready to use again.
    /// The associated operation is returned in the form of a
    /// [`StaleMultiChannelOperation`]. This must be refreshed to recover the
    /// useable [`MultiChannelOperation`].
    ///
    /// On error, a [`MultiChannelTransferError`] will be returned which
    /// encapsulates the error information, as well as the [Device] and the
    /// associated [`StaleMultiChannelOperation`].
    pub async fn completed(
        mut self,
        timeout_duration: time::Duration,
    ) -> Result<(Device, StaleMultiChannelOperation, MemBlock), MultiChannelTransferError> {
        // Call inner_completed which borrows self mutably, so the usual ?
        // semantics work fine and we can still destructure self afterwards.
        let res = self.inner_completed(timeout_duration).await;
        let Self {
            device,
            operation,
            mem_block,
        } = self;

        match res {
            Err(err) => Err(MultiChannelTransferError {
                source: err,
                errored_device: (device, operation.into()).into(),
            }),
            Ok(()) => Ok((device, operation.into(), mem_block)),
        }
    }
}

#[derive(Debug)]
pub struct ErroredRunningDevice {
    pub(super) device: Device,
    pub(super) stale_operation: StaleMultiChannelOperation,
}

impl From<RunningDevice> for ErroredRunningDevice {
    fn from(source: RunningDevice) -> Self {
        let RunningDevice {
            device,
            operation,
            mem_block: _,
        } = source;

        ErroredRunningDevice {
            device,
            stale_operation: operation.into(),
        }
    }
}

impl From<(Device, MultiChannelOperation)> for ErroredRunningDevice {
    fn from((device, operation): (Device, MultiChannelOperation)) -> Self {
        ErroredRunningDevice {
            device,
            stale_operation: operation.into(),
        }
    }
}

impl ErroredRunningDevice {
    /// Resets the errored device and refreshes the stale operation,
    /// returning the encapsulated device, the operation, and any errors
    /// reported through the descriptors.
    ///
    /// Errors reported through the descriptors are returned as the third
    /// element in the return tuple. This is useful for diagnosing _why_ the
    /// device errored. If no error was found then it will be `None`.
    pub fn reset(
        self,
    ) -> Result<
        (
            Device,
            MultiChannelOperation,
            Option<Vec<(Channel, OperationError)>>,
        ),
        DeviceError,
    > {
        let ErroredRunningDevice {
            mut device,
            stale_operation,
        } = self;

        device.reset()?;

        match stale_operation.check_and_refresh() {
            Err(MultiChannelOperationRefreshError {
                errors,
                refreshed_operation,
            }) => Ok((device, refreshed_operation, Some(errors))),
            Ok(refreshed_operation) => Ok((device, refreshed_operation, None)),
        }
    }

    /// Unwraps an ErroredRunningDevice into the [`Device`] and the
    /// [`StaleMultiChannelOperation`] from which the error occurred.
    ///
    /// Strictly speaking, the process of unwrapping is not unsafe, but the
    /// returned [`Device`] is in an undefined state. It is probably necessary
    /// to manually reset the device before it can be used again. Using it
    /// before it has been reset will result in undefined behaviour.
    pub unsafe fn unwrap(self) -> (Device, StaleMultiChannelOperation) {
        let ErroredRunningDevice {
            device,
            stale_operation,
        } = self;

        (device, stale_operation)
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod device_tests {

    use crate::{
        BusDataWidth,
        errors::DeviceError,
        multi_channel::{Channel, Device, MultiChannelOperation, MultiChannelTransferError},
    };
    use serial_test::serial;
    use std::{collections::HashMap, path::PathBuf, time};

    /// When a running device times out, it should return a
    /// MultiChannelTransferError { DeviceError::TimedOut, ... }. The errored
    /// device (including the operation) once reset should be ready to use
    /// again.
    #[test]
    #[serial]
    fn test_multi_channel_operation_timing_out() {
        // Create devices
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        let mut s2mm_device = Device::new(&s2mm_device_path, BusDataWidth::FourBytes).unwrap();
        let mut mm2s_device = Device::new(&mm2s_device_path, BusDataWidth::FourBytes).unwrap();

        // Create some trivial operations
        let s2mm_operation_0 = s2mm_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        let mm2s_operation_0 = mm2s_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        // Create the operations hashmaps
        let mut s2mm_operations = HashMap::new();
        s2mm_operations.insert(Channel::new(0_u8).unwrap(), s2mm_operation_0);

        let mut mm2s_operations = HashMap::new();
        mm2s_operations.insert(Channel::new(0_u8).unwrap(), mm2s_operation_0);

        let s2mm_operations = MultiChannelOperation::new(s2mm_operations).unwrap();
        let mm2s_operations = MultiChannelOperation::new(mm2s_operations).unwrap();

        // Firstly run the s2mm operation with no data, which should timeout
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();

        let timeout = time::Duration::from_millis(100);

        // Wait for DMA transfers to complete
        let errored_device = match s2mm_running_operation.await_completed(timeout) {
            Err(MultiChannelTransferError {
                source: DeviceError::TimedOut,
                errored_device,
            }) => errored_device,
            Err(e) => panic!("Received the wrong error (should be TimedOut): {:?}", e),
            Ok(_) => panic!("The operation should have errored."),
        };

        let (s2mm_device, s2mm_operations, errs) = errored_device.reset().unwrap();

        match errs {
            None => panic!("The checking of the descriptors should have errored."),
            Some(_) => (),
        }
        // Now we should be able to try again...
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();
        let mm2s_running_operation = mm2s_device.do_dma(mm2s_operations).unwrap();

        let timeout = time::Duration::from_millis(100);

        // This should work fine without error.
        s2mm_running_operation
            .await_completed(timeout.clone())
            .unwrap();
        mm2s_running_operation.await_completed(timeout).unwrap();
    }
}

#[cfg(all(test, target_arch = "arm", feature = "async"))]
mod async_device_tests {
    use crate::{
        BusDataWidth,
        errors::DeviceError,
        multi_channel::{Channel, Device, MultiChannelOperation, MultiChannelTransferError},
    };
    use serial_test::serial;
    use std::{collections::HashMap, path::PathBuf, time};

    use tokio;

    /// When a running device times out, it should return a
    /// MultiChannelTransferError { DeviceError::TimedOut, ... }. The errored
    /// device (including the operation) once reset should be ready to use
    /// again.
    #[tokio::test]
    #[serial]
    async fn test_multi_channel_operation_timing_out() {
        // Create devices
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        let mut s2mm_device = Device::new(&s2mm_device_path, BusDataWidth::FourBytes).unwrap();
        let mut mm2s_device = Device::new(&mm2s_device_path, BusDataWidth::FourBytes).unwrap();

        // Create some trivial operations
        let s2mm_operation_0 = s2mm_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        let mm2s_operation_0 = mm2s_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        // Create the operations hashmaps
        let mut s2mm_operations = HashMap::new();
        s2mm_operations.insert(Channel::new(0_u8).unwrap(), s2mm_operation_0);

        let mut mm2s_operations = HashMap::new();
        mm2s_operations.insert(Channel::new(0_u8).unwrap(), mm2s_operation_0);

        let s2mm_operations = MultiChannelOperation::new(s2mm_operations).unwrap();
        let mm2s_operations = MultiChannelOperation::new(mm2s_operations).unwrap();

        // Firstly run the s2mm operation with no data, which should timeout
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();

        let timeout = time::Duration::from_millis(100);

        // Wait for DMA transfers to complete
        let errored_device = match s2mm_running_operation.completed(timeout).await {
            Err(MultiChannelTransferError {
                source: DeviceError::TimedOut,
                errored_device,
            }) => errored_device,
            Err(e) => panic!("Received the wrong error (should be TimedOut): {:?}", e),
            Ok(_) => panic!("The operation should have errored."),
        };

        let (s2mm_device, s2mm_operations, errs) = errored_device.reset().unwrap();

        match errs {
            None => panic!("The checking of the descriptors should have errored."),
            Some(_) => (),
        }
        // Now we should be able to try again...
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();
        let mm2s_running_operation = mm2s_device.do_dma(mm2s_operations).unwrap();

        let timeout = time::Duration::from_millis(100);

        // This should work fine without error.
        s2mm_running_operation
            .completed(timeout.clone())
            .await
            .unwrap();
        mm2s_running_operation.completed(timeout).await.unwrap();
    }
}
