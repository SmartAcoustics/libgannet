/// Defines the maximum number of bytes that can be transferred by each
/// descriptor. Note: This assumes that the Width of Buffer Length Register in
/// the DMA engine in the PL is set to 23 bits. If this is not the case then
/// this library will not behave as expected.
pub const MAX_BYTES_TRANSFER_PER_DESCRIPTOR: usize = (1 << 23) - 1;

pub(crate) enum DescriptorFields {
    NextDescriptorLsw,
    NextDescriptorMsw,
    BufferAddrLsw,
    BufferAddrMsw,
    Control,
    Status,
    _App0,
    _App1,
    _App2,
    _App3,
    _App4,
}
impl DescriptorFields {
    // offset gives the offset in bytes
    pub(crate) fn offset(&self) -> usize {
        match *self {
            DescriptorFields::NextDescriptorLsw => 0x00,
            DescriptorFields::NextDescriptorMsw => 0x04,
            DescriptorFields::BufferAddrLsw => 0x08,
            DescriptorFields::BufferAddrMsw => 0x0C,
            DescriptorFields::Control => 0x18,
            DescriptorFields::Status => 0x1C,
            DescriptorFields::_App0 => 0x20,
            DescriptorFields::_App1 => 0x24,
            DescriptorFields::_App2 => 0x28,
            DescriptorFields::_App3 => 0x2C,
            DescriptorFields::_App4 => 0x30,
        }
    }
}

pub(crate) enum DescriptorControlField {
    _BufferLength, // MM2S: Transfer length. S2MM: Available memory
    EndOfFrame,    // Indicates end of packet
    StartOfFrame,  // Indicated start of packet
}
impl DescriptorControlField {
    pub(crate) fn bitmask(&self) -> u32 {
        match *self {
            DescriptorControlField::_BufferLength => 2_u32.pow(25) - 1,
            DescriptorControlField::EndOfFrame => 1 << 26,
            DescriptorControlField::StartOfFrame => 1 << 27,
        }
    }
}

pub(crate) enum DescriptorStatusField {
    TransferredBytes, // The number of bytes transferred by this descriptor
    _EndOfFrame,      // Indicates end of packet
    _StartOfFrame,    // Indicated start of packet
    InternalError,    // DMA internal error
    SlaveError,       // DMA slave error
    DecodeError,      // DMA decode error
    Complete,         // DMA engine has completed transfer described by this descriptor
}
impl DescriptorStatusField {
    pub(crate) fn bitmask(&self) -> u32 {
        match *self {
            DescriptorStatusField::TransferredBytes => 2_u32.pow(25) - 1,
            DescriptorStatusField::_EndOfFrame => 1 << 26,
            DescriptorStatusField::_StartOfFrame => 1 << 27,
            DescriptorStatusField::InternalError => 1 << 28,
            DescriptorStatusField::SlaveError => 1 << 29,
            DescriptorStatusField::DecodeError => 1 << 30,
            DescriptorStatusField::Complete => 1 << 31,
        }
    }
}
