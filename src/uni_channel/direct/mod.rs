mod running;
pub use running::{ErroredRunningDevice, RunningDevice};

use std::{
    fs::File,
    ops,
    path::PathBuf,
    sync::atomic::{AtomicU64, Ordering},
    time,
};

use thiserror::Error;

use crate::{device_core::DeviceSpecifics, BusDataWidth};
use crate::errors::{DeviceError, DirectDmaError, DmaCoreError};
use crate::mem_setup::setup_cfg_and_data_mem;
use crate::uni_channel::{
    registers::ControlRegister, registers::Registers, UniChannelDeviceCommon,
};
use crate::utils::{enable_interrupts, get_and_consume_interrupts, open_device};
use crate::TransferType;
use crate::{MemBlock, MemBlockLayout, MemPool};

#[cfg(feature = "async")]
use tokio::io::unix::AsyncFd;

/// This provides a globally unique ID for each device that is created.
/// The index is incremented on each read. This is fine - if a billion
/// devices are created every second, the program could run for nearly
/// 600 years before the counter wraps around.
static NEXT_DEVICE_ID: AtomicU64 = AtomicU64::new(0);

// FIXME I suggest we need a time out type that returns TimedoutRunningDevice
// which can be resumed, or converted into an ErroredRunningDevice if resumption
// is undesirable.
/// Error during a direct DMA transfer. This error type encapsulates
/// the device that is recovered
#[derive(Error, Debug)]
pub enum DirectTransferError {
    /// Transfer was cancelled through the cancellation token
    #[error("Transfer was cancelled")]
    TransferCancelled { device: Device },
    /// Device error
    #[error("Device error: {source}")]
    DeviceError {
        errored_device: ErroredRunningDevice,
        source: DeviceError,
    },
    /// Incorrect number of bytes transferred, though those that were _could_
    /// be correct (for example, if an S2MM packet was smaller than the
    /// expected size, it is likely the case that the whole packet was
    /// transferred). In general, it is necessary to have an insight into
    /// what situations will cause the DMA engine to fail to transfer the
    /// expected number of bytes in order to know whether the data transferred
    /// is valid.
    #[error("Incorrect number of bytes transferred: {transferred} (expected {expected})")]
    TransferSize {
        errored_device: ErroredRunningDevice,
        transferred: usize,
        expected: usize,
    },
    /// Unrecoverable device error
    #[error("Device error: {source}")]
    UnrecoverableDeviceError { source: DeviceError },
}

/// A pre-checked MemBlock that can be passed directly to
/// [Device::do_dma_with_mem]. It behaves like [MemBlock] in that it
/// deferences to a slice.
pub struct CheckedMemBlock {
    memory: MemBlock,
    device_id: u64,
    offset: usize,
    size: usize,
}

impl CheckedMemBlock {
    /// Create a new `CheckedMemBlock`, checking bounds and size constraints
    /// against those required by `device`.
    pub fn new(
        device: &mut Device,
        offset: usize,
        size: usize,
    ) -> Result<CheckedMemBlock, DeviceError> {
        // Check that the transfer size is valid. 1<<23 is used here as this
        // is the maximum length the DMA engine can be set to take in Vivado.
        if size >= 1 << 23 {
            Err(DirectDmaError::InvalidSize)?
        }

        if size % device.bus_width() as usize != 0 {
            Err(DirectDmaError::InvalidSize)?
        }

        let mem_size = device.dma_mem_pool.size();

        // Check that the offset is valid. Offset cannot be less than 0 due to
        // type limits.
        if offset >= mem_size || offset % device.bus_width() as usize != 0 {
            Err(DirectDmaError::InvalidOffset)?
        }

        // Check that the combination of offset and size is valid
        if offset + size > mem_size {
            Err(DirectDmaError::MemoryOverflow)?
        }

        let memory = device
            .dma_mem_pool
            .check_out_block(MemBlockLayout { offset, size })?;

        let checked_block = CheckedMemBlock {
            memory,
            device_id: device.device_id,
            offset,
            size,
        };

        Ok(checked_block)
    }
}

impl ops::Deref for CheckedMemBlock {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        &*self.memory
    }
}

impl ops::DerefMut for CheckedMemBlock {
    fn deref_mut(&mut self) -> &mut [u8] {
        &mut *self.memory
    }
}

impl From<CheckedMemBlock> for MemBlock {
    fn from(checked_block: CheckedMemBlock) -> Self {
        checked_block.memory
    }
}

/// The Device structure encapsulates all the information required for the
/// system to perform direct DMA operations. There are methods on the Device
/// structure which enable these operations.
#[derive(Debug)]
pub struct Device {
    unichannel_common: UniChannelDeviceCommon,
    dma_mem_pool: MemPool,
    dma_mem_phys_addr: usize,
    device_id: u64,
    file: File,
    bus_width: BusDataWidth,
    #[cfg(feature = "async")]
    async_file: Option<AsyncFd<File>>,
}

impl Drop for Device {
    fn drop(&mut self) {
        // Reset the underlying device on drop. Ignore any errors as the
        // device is being dropped anyway.
        let _ = self.reset();
    }
}

// Implement the DeviceSpecifics trait
impl DeviceSpecifics for Device {
    fn write_reset_bit(&self) {
        self.unichannel_common.write_reset_bit();
    }

    fn halted(&self) -> bool {
        self.unichannel_common.halted()
    }

    fn idle(&self) -> bool {
        self.unichannel_common.idle()
    }

    fn check_error(&self) -> Result<(), DmaCoreError> {
        self.unichannel_common.check_error()
    }

    fn get_and_consume_interrupts(
        &mut self,
        timeout: &time::Duration,
    ) -> Result<usize, DeviceError> {
        get_and_consume_interrupts(&mut self.file, timeout)
    }
}

impl Device {
    /// A function to create the device and get it ready for use.
    ///
    /// This function should be run first before attempting any DMA transfers.
    ///
    /// In the case of failure this function will return the causing error.
    pub fn new(device_path: &PathBuf, bus_width: BusDataWidth)
        -> Result<Device, DeviceError>
    {
        // Initialise the device
        let mut device = Device::init(&device_path, bus_width)?;

        // Reset the device
        device.reset()?;

        Ok(device)
    }

    /// A function to initiate the direct DMA device.
    ///
    /// Given a system file, this function extracts all the required
    /// information and returns a direct DMA device.
    fn init(device_path: &PathBuf, bus_width: BusDataWidth)
        -> Result<Device, DeviceError>
    {
        // Open the device file with the right settings
        let file = open_device(device_path)?;

        let (cfg_mem, dma_data_mem, dma_mem_phys_addr) =
            setup_cfg_and_data_mem::<Registers>(device_path, &file)?;

        let unichannel_common = UniChannelDeviceCommon::new(cfg_mem)?;

        if unichannel_common.scatter_gather_enabled {
            // Error if the device is scatter gather enabled
            return Err(DeviceError::UnsupportedScatterGather);
        }

        let dma_mem_pool = MemPool::new(dma_data_mem);

        let device_id = NEXT_DEVICE_ID.fetch_add(1, Ordering::Relaxed);

        Ok(Device {
            unichannel_common,
            dma_mem_pool,
            dma_mem_phys_addr,
            device_id,
            file,
            bus_width,
            #[cfg(feature = "async")]
            async_file: None,
        })
    }

    fn do_dma_with_mem_inner(
        &mut self,
        mem_block: CheckedMemBlock,
    ) -> Result<MemBlock, DeviceError> {
        if mem_block.device_id != self.device_id {
            return Err(DeviceError::WrongDevice);
        }

        let offset = mem_block.offset;
        let size = mem_block.size;
        let mem_block: MemBlock = mem_block.into();

        if size == 0 {
            // Short circuit the zero length case (which will not run or
            // interrupt in any case).
            return Ok(mem_block)
        }

        // The DMA device has two 32 bit registers to set the destination or
        // source address for the transfer. One takes the most significant
        // word of the address (address_msw), the other the least significant
        // word (address_lsw). Note: this means the DMA device can only handle
        // address ranges up to 2**64.
        //
        // If we are on a 64 bit architecture casting to u32 takes the bottom
        // 32 bits. If the architecture is 32 bits or smaller then casting to
        // u32 will take the whole number.
        let address_lsw: u32 = (self.dma_mem_phys_addr + offset) as u32;
        // If it is a 64 bit architecture we need to get the most significant
        // word of the address. To get this we cast to u64, shift it down by
        // 32 bits then cast that to u32. If the architecture is 32 bit or
        // smaller than the msw of the address will just be zero.
        let address_msw: u32 = ((self.dma_mem_phys_addr + offset) as u64 >> 32) as u32;

        enable_interrupts(&mut self.file)?;

        // Determine which ctrl, address and length registers need to be
        // used depending on the transfer type.
        let (ctrl_reg, address_lsw_reg, address_msw_reg, length_reg) =
            match self.unichannel_common.transfer_type {
                TransferType::MM2S => (
                    Registers::Mm2sControlRegister,
                    Registers::Mm2sStartAddressLsw,
                    Registers::Mm2sStartAddressMsw,
                    Registers::Mm2sLength,
                ),
                TransferType::S2MM => (
                    Registers::S2mmControlRegister,
                    Registers::S2mmDestAddressLsw,
                    Registers::S2mmDestAddressMsw,
                    Registers::S2mmLength,
                ),
            };

        // SET THE DMA CONFIG REGISTER FOR THE RUN
        // Set:
        //     run bit
        //     interrupt on complete bit
        //     interrupt on error bit
        // All other config bits are set to 0, except IRQThreshold, which
        // implicitly acquires the default value of 1.
        self.unichannel_common.write_reg(
            ctrl_reg,
            ControlRegister::RunStop.bitmask()
                | ControlRegister::IocIrqEn.bitmask()
                | ControlRegister::ErrIrqEn.bitmask(),
        );

        // Set the source/destination address of the transfer
        self.unichannel_common
            .write_reg(address_lsw_reg, address_lsw);
        self.unichannel_common
            .write_reg(address_msw_reg, address_msw);

        // BEGIN THE RUN
        // Write the number of bytes to transfer. This initiates the transfer.
        // We can safely cast to u32 here as we have already checked that
        // size is valid.
        self.unichannel_common.write_reg(length_reg, size as u32);

        Ok(mem_block)
    }

    /// A function to do a DMA transfer.
    ///
    /// It checks that the arguments passed to it are valid for the device
    /// and returns errors if not.
    ///
    /// If successful this function returns a [RunningDevice], which can be
    /// waited on for completion of the DMA.
    ///
    /// Note: offset should be given in bytes.
    ///
    /// Note: The Mm2sLength and S2mmLength registers
    /// can take a length of up to 2<sup>26</sup> . However in Vivado where
    /// we create the DMA engine, the maximum length it can handle is 2<sup>23</sup>.
    /// This is probably for forward compatibility reason. An error will be
    /// returned if the length is bigger than 2<sup>23</sup>.
    ///
    /// It is possible to set the maximum length lower than 2<sup>23</sup> in
    /// Vivado in which case this library will not detect the issue. It is up
    /// to the user to ensure the nbytes passed to this function will work
    /// with the DMA engine in their PL.
    pub fn do_dma(
        mut self,
        offset: usize,
        size: usize,
    ) -> Result<RunningDevice, DirectTransferError> {
        let mut do_dma_inner = |offset: usize, size: usize| {
            let mem_block = self.get_checked_memory(offset, size)?;

            self.do_dma_with_mem_inner(mem_block)
        };

        match do_dma_inner(offset, size) {
            Ok(mem_block) => Ok(RunningDevice {
                device: self,
                mem_block,
                expected_bytes: size,
            }),
            Err(e) => Err(DirectTransferError::DeviceError {
                errored_device: self.into(),
                source: e,
            }),
        }
    }

    /// Performs a DMA transfer in which the memory block has been pre-claimed.
    /// This is useful for MM2S transfers in which you want an unbroken
    /// ownership chain of the memory.
    ///
    /// Beyond the signature, it behaves exactly like [do_dma].
    ///
    /// It checks that the arguments passed to it are valid for the device
    /// and returns errors if not.
    ///
    /// If successful this function returns a [RunningDevice], which can be
    /// waited on for completion of the DMA.
    ///
    /// Note: offset should be given in bytes.
    ///
    /// Note: The Mm2sLength and S2mmLength registers
    /// can take a length of up to 2<sup>26</sup> . However in Vivado where
    /// we create the DMA engine, the maximum length it can handle is 2<sup>23</sup>.
    /// This is probably for forward compatibility reason. An error will be
    /// returned if the length is bigger than 2<sup>23</sup>.
    ///
    /// It is possible to set the maximum length lower than 2<sup>23</sup> in
    /// Vivado in which case this library will not detect the issue. It is up
    /// to the user to ensure the nbytes passed to this function will work
    /// with the DMA engine in their PL.
    pub fn do_dma_with_mem(
        mut self,
        mem_block: CheckedMemBlock,
    ) -> Result<RunningDevice, DirectTransferError> {
        let size = mem_block.size;

        match self.do_dma_with_mem_inner(mem_block) {
            Ok(mem_block) => Ok(RunningDevice {
                device: self,
                mem_block,
                expected_bytes: size,
            }),
            Err(e) => Err(DirectTransferError::DeviceError {
                errored_device: self.into(),
                source: e,
            }),
        }
    }

    /// Like [get_memory], but checks the offset and size are suitable for
    /// the DMA transfer before being returned. If there is a problem, a
    /// suitable error will be returned.
    pub fn get_checked_memory(
        &mut self,
        offset: usize,
        size: usize,
    ) -> Result<CheckedMemBlock, DeviceError> {
        CheckedMemBlock::new(self, offset, size)
    }

    /// Return a memory block from the device memory pool.
    ///
    /// It is not possible to have live overlapping memory blocks. Until
    /// a memory block is freed, this method will error if an overlapping
    /// block is requested.
    ///
    /// Once the memory block is dropped, it relinquishes control of that
    /// region of memory.
    ///
    /// The returned memory block is both [Send] and [Sync] so can be passed
    /// around as needed.
    ///
    /// Note that when [do_dma] is called, it will first attempt to get hold
    /// of the necessary bit of memory, which will cause an error if the
    /// user owns an overlapping block.
    pub fn get_memory(&mut self, offset: usize, size: usize) -> Result<MemBlock, DeviceError> {
        Ok(self
            .dma_mem_pool
            .check_out_block(MemBlockLayout { offset, size })?)
    }

    /// Returns the total size of the device memory pool.
    ///
    /// This number has nothing to do with the size of the blocks that can be
    /// checked out at this moment in time, which depends on what other blocks
    /// have been checked out. It is literally the total size of the memory
    /// pool.
    pub fn memory_pool_size(&self) -> usize {
        self.dma_mem_pool.size()
    }

    /// Return the configured bus width of this device.
    pub fn bus_width(&self) -> BusDataWidth {
        self.bus_width
    }

    /// This function prints the status of the device in human readable form.
    pub fn print_status(&self) {
        self.unichannel_common.print_status();
    }

    pub fn reset(&mut self) -> Result<(), DeviceError> {
        <Device as DeviceSpecifics>::reset(self)
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod tests {

    use rand::{distributions::Standard, rngs::SmallRng, Rng, SeedableRng};
    use serial_test::serial;
    use std::{iter::once, ops::Range, path::PathBuf};

    use crate::BusDataWidth;

    use super::{
        CheckedMemBlock, Device, DeviceError, DirectDmaError, DirectTransferError, MemBlock,
    };

    /// The `dma::direct::device::new` function should return an
    /// UnsupportedScatterGather error if the underlying device is a scatter
    /// gather DMA device.
    #[test]
    #[serial]
    fn test_unsupported_scatter_gather() {
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();

        let device = Device::new(
            &device_path, BusDataWidth::FourBytes);

        match device {
            Err(DeviceError::UnsupportedScatterGather) => (),
            _ => panic!("Did not get DeviceError::UnsupportedScatterGather error."),
        }
    }

    /// The `dma::direct::device::new` function should return an
    /// UnsupportedCapability error if the underlying device is capable of
    /// performing both MM2S and S2MM.
    #[test]
    #[serial]
    fn test_unsupported_capability() {
        let device_path: PathBuf = ["/dev", "axi_dma_combined"].iter().collect();

        let device = Device::new(
            &device_path, BusDataWidth::FourBytes);

        match device {
            Err(DeviceError::UnsupportedCapability) => (),
            _ => panic!("Did not get DeviceError::UnsupportedCapability error."),
        }
    }

    /// The `dma::direct::device::new` function should return an
    /// UnsupportedMultiChannel error if the underlying device is a multi
    /// channel device.
    #[test]
    #[serial]
    fn test_unsupported_multi_channel() {
        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        let device = Device::new(
            &device_path, BusDataWidth::FourBytes);

        match device {
            Err(DeviceError::UnsupportedMultiChannel) => (),
            _ => panic!("Did not get DeviceError::UnsupportedMultiChannel error."),
        }
    }


    /// The `dma::direct::device::do_dma` function should check that
    /// the `nbytes` argument is in the range 0 -> 2**LENGTH_BITFIELD_LENGTH
    /// and that it is a multiple of the bus width.
    /// If it is not, then it should return an `InvalidSize` error.
    #[test]
    #[serial]
    fn test_invalid_size() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();
        let mut device = Device::new(
            &device_path, BusDataWidth::FourBytes).unwrap();

        let mut rng = SmallRng::from_entropy();
        let mut rng2 = SmallRng::from_entropy();

        let nbytes_vec: Vec<usize> = once((1 << device.bus_width() as usize) + 1)
            .chain((0..50)
                .map(|_| rng.gen_range(0..=(1<<23)))
                .filter(|nbytes| nbytes % device.bus_width() as usize != 0)
            )
            .chain((0..20).map(|_| {
                rng2.gen_range((1<<23) + device.bus_width() as usize..usize::MAX)
            })).collect();

        for nbytes in nbytes_vec {
            let offset: usize = 0;
            device = match device.do_dma(offset, nbytes).unwrap_err() {
                DirectTransferError::DeviceError {
                    errored_device,
                    source:
                        DeviceError::DirectDma {
                            source: DirectDmaError::InvalidSize,
                        },
                } => errored_device.reset().unwrap(),
                val => {
                    panic!("Did not get InvalidSize error. Instead got {:?}", val)
                },
            };
        }
    }

    /// The `dma::direct::device::do_dma` function should check that
    /// the `offset` argument is in the range 0 -> `dma_data_size` and that it
    /// is a multiple of the bus width. If either of these is not
    /// true then it should return a `InvalidOffset` error.
    #[test]
    #[serial]
    fn test_invalid_offset() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let nbytes: usize = 16;
        let mut rng = SmallRng::from_entropy();
        let mut rng2 = SmallRng::from_entropy();

        let offset_vec: Vec<usize> = once(device.memory_pool_size())
            .chain((0..20).map(|_| rng.gen_range(device.memory_pool_size()..usize::MAX)))
            .chain((0..50)
                .map(|_| rng2.gen_range(0..device.memory_pool_size()))
                .filter(|offset| offset % device.bus_width() as usize != 0)
            )
            .collect();

        for offset in offset_vec {

            device = match device.do_dma(offset, nbytes).unwrap_err() {
                DirectTransferError::DeviceError {
                    errored_device,
                    source:
                        DeviceError::DirectDma {
                            source: DirectDmaError::InvalidOffset,
                        },
                } => errored_device.reset().unwrap(),
                val => {
                    panic!("Did not get InvalidSize error. Instead got {:?}", val)
                },
            };
        }
    }

    /// The `dma::direct::device::do_dma` function should check that
    /// the `nbytes` and `offset` arguments will not sum to greater than
    /// `dma_data_size`. If they do then it would cause a memory overflow. The
    /// `dma::device::do_dma` function should prevent this by returning
    /// a `MemoryOverflow` error.
    #[test]
    #[serial]
    fn test_memory_overflow() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();
        let mut rng = SmallRng::from_entropy();

        let bus_width = device.bus_width() as usize;

        for _ in 0..40 {
            // Generate a random nbytes
            // Create a valid nbytes.
            let nbytes: usize = bus_width * rng.gen_range(1..(1 << 23)/bus_width);
            // Create a valid offset that will cause the memory to overflow
            let arb_offset: usize = rng
                .gen_range((device.memory_pool_size() - nbytes + bus_width)..device.memory_pool_size());
            // force offset to be a multiple of bus_width
            let offset = bus_width * (arb_offset/bus_width);

            device = match device.do_dma(offset, nbytes).unwrap_err() {
                DirectTransferError::DeviceError {
                    errored_device,
                    source:
                        DeviceError::DirectDma {
                            source: DirectDmaError::MemoryOverflow,
                        },
                } => errored_device.reset().unwrap(),
                val => {
                    panic!("Did not get MemoryOverflow error. Instead got {:?}", val)
                }
            }
        }
    }

    /// A CheckedMemBlock should be accessible as though it was the separate
    /// MemBlock.
    #[test]
    #[serial]
    fn test_checked_mem_block() {
        // Create device 0
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let mut rng = SmallRng::from_entropy();
        let mut ref_data: Vec<u8> = (&mut rng)
            .sample_iter(&Standard)
            .take(device.memory_pool_size())
            .collect();

        let offset = 32;
        let size = 1024;

        let ref_slicer: Range<usize> = offset..(size + offset);

        {
            let mut mem = device.get_memory(0, device.memory_pool_size()).unwrap();
            mem.copy_from_slice(&ref_data);
        }

        {
            let mem = device.get_checked_memory(offset, size).unwrap();
            assert_eq!(*mem, *ref_data.get(ref_slicer.clone()).unwrap());
        }

        {
            let mem = CheckedMemBlock::new(&mut device, offset, size).unwrap();
            assert_eq!(*mem, *ref_data.get(ref_slicer.clone()).unwrap());

            let mem: MemBlock = mem.into();
            assert_eq!(*mem, *ref_data.get(ref_slicer.clone()).unwrap());
        };

        // Write through one way of getting the memory
        {
            let mut mem = device.get_checked_memory(offset, size).unwrap();
            let mem_len = mem.len();
            mem.copy_from_slice(&vec![0u8; mem_len]);
        }

        // And then through another way.
        {
            let mut mem = CheckedMemBlock::new(&mut device, offset, size).unwrap();
            let mem_len = mem.len();
            mem.copy_from_slice(&vec![1u8; mem_len]);

            // Update the reference
            let sliced_ref_data = ref_data.get_mut(ref_slicer.clone()).unwrap();
            sliced_ref_data.copy_from_slice(&vec![1u8; mem_len]);
        }

        // Finally check the memory is still consistent
        let mem = device.get_memory(0, device.memory_pool_size()).unwrap();
        assert_eq!(*mem, *ref_data);
    }
}
