use std::fs;
use std::io;
use std::num;
use std::{
    fs::File,
    mem::size_of,
    path::{Path, PathBuf},
};

use memmap2::{MmapMut, MmapOptions};
use strum::IntoEnumIterator;

use thiserror::Error;

const UIO_PATH: &'static str = "/sys/class/uio";

// These three paths rely on the driver mapping:
// cfg = map0
// dma_data = map1
// dma_descriptors = map2
// If the driver is modified these may change. We check this and will return
// an error if the mappings change.
const CFG_PATH: &'static str = "maps/map0";
const DMA_DATA_PATH: &'static str = "maps/map1";
const DMA_DESCRIPTORS_PATH: &'static str = "maps/map2";

const SIZE_PATH: &'static str = "size";
const ADDR_PATH: &'static str = "addr";
const NAME_PATH: &'static str = "name";

/// Error in setting up the device memory maps.
#[derive(Error, Debug)]
pub enum MemSetupError {
    /// IO error from external function.
    #[error("IO error: {source}")]
    Io {
        #[from]
        source: io::Error,
    },
    /// Error parsing the number string.
    #[error("Error parsing the number string: {source}")]
    NumParseInt {
        #[from]
        source: num::ParseIntError,
    },
    /// The memory map for the register space is too small for the register layout.
    #[error("The memory map for the register space is too small for the register layout.")]
    RegisterMemoryTooSmall,
    /// The memory map device is not set up as expected. It may be an incorrect device.
    #[error("The memory map device is not set up as expected. It may be an incorrect device.")]
    InvalidMemMap,
}

pub(crate) trait RegisterLayout {
    type RegisterType;

    fn offset(&self) -> usize;
}

pub(crate) trait RegisterCheck: IntoEnumIterator + RegisterLayout {
    /// Returns the number of bytes required to fit the register layout in
    /// memory.
    #[must_use]
    fn memory_required() -> usize {
        let mut max_offset = 0usize;
        for each in Self::iter() {
            let each_offset = each.offset();
            if each_offset > max_offset {
                max_offset = each_offset;
            }
        }

        max_offset * size_of::<Self::RegisterType>()
    }
}
/// This function sets up the cfg and data memory for the device.
/// RegisterMap is the type used to represent the register layout and is
/// needed to make sure the register layout fits inside the register space.
pub(crate) fn setup_cfg_and_data_mem<R: RegisterCheck>(
    device_path: &PathBuf,
    file: &File,
) -> Result<(MmapMut, MmapMut, usize), MemSetupError> {
    // Read the symbolic link to get the path of the underlying file
    let uio_name = fs::read_link(device_path)?;

    // Set up the path to the file specifying the size of the configuration
    // register space
    let cfg_size_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(CFG_PATH),
        Path::new(SIZE_PATH),
    ]
    .iter()
    .collect();
    // Read the size of the configuration register space
    let cfg_size = fs::read_to_string(cfg_size_fp)?;

    // Trim the leading 0x and the leading & trailing whitespace from the
    // cfg_size then convert to an integer
    let cfg_size = usize::from_str_radix(cfg_size.trim_start_matches("0x").trim(), 16)?;

    if R::memory_required() > cfg_size {
        return Err(MemSetupError::RegisterMemoryTooSmall);
    }
    // Memory map the configuration register
    let cfg_mem = unsafe { MmapOptions::new().offset(0).len(cfg_size).map_mut(&*file)? };

    // Set up the path to the file specifying the name of the DMA data memory
    // space
    let dma_data_name_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DATA_PATH),
        Path::new(NAME_PATH),
    ]
    .iter()
    .collect();
    // Read the name of underlying memory
    let dma_data_name = fs::read_to_string(dma_data_name_fp)?;
    // Trim trailing whitespace
    let dma_data_name = dma_data_name.trim();
    // Check that the name of the underlying memory is dma_data. This is a
    // sanity check to make sure we have the correct memory. If the driver
    // changes the order of the mappings then this function will return an
    // error
    match dma_data_name.as_ref() {
        "dma_data" => (),
        _ => return Err(MemSetupError::InvalidMemMap),
    }

    // Set up the path to the file specifying the size of the DMA data
    // memory space
    let dma_data_size_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DATA_PATH),
        Path::new(SIZE_PATH),
    ]
    .iter()
    .collect();
    // Read the size of the DMA_DATA memory space
    let dma_data_size = fs::read_to_string(dma_data_size_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_data_size then convert to an integer.
    let dma_data_size = usize::from_str_radix(dma_data_size.trim_start_matches("0x").trim(), 16)?;

    // Memory map the DMA_DATA memory
    let dma_data_mem = unsafe {
        MmapOptions::new()
            .offset(page_size::get() as u64)
            .len(dma_data_size)
            .map_mut(&*file)?
    };

    // Set up the path to the file specifying the physical address of the
    // DMA_DATA memory space
    let dma_data_phys_addr_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DATA_PATH),
        Path::new(ADDR_PATH),
    ]
    .iter()
    .collect();
    // Read the physical address of the DMA_DATA memory space. This is set up
    // by the kernel and written to a file so we can access it.
    let dma_data_phys_addr = fs::read_to_string(dma_data_phys_addr_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_data_phys_addr then convert to an integer.
    let dma_data_phys_addr =
        usize::from_str_radix(dma_data_phys_addr.trim_start_matches("0x").trim(), 16)?;

    Ok((cfg_mem, dma_data_mem, dma_data_phys_addr))
}

/// This function sets up the descriptor memory for the device.
pub(crate) fn setup_descriptors_mem(
    device_path: &PathBuf,
    file: &File,
) -> Result<(MmapMut, usize), MemSetupError> {
    // Read the symbolic link to get the path of the underlying file
    let uio_name = fs::read_link(device_path)?;

    // Set up the path to the file specifying the name of the DMA
    // descriptors memory space
    let dma_descriptors_name_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DESCRIPTORS_PATH),
        Path::new(NAME_PATH),
    ]
    .iter()
    .collect();
    // Read the name of underlying memory
    let dma_descriptors_name = fs::read_to_string(dma_descriptors_name_fp)?;
    // Trim trailing whitespace
    let dma_descriptors_name = dma_descriptors_name.trim();
    // Check that the name of the underlying memory is dma_descriptors.
    // This is a sanity check to make sure we have the correct memory. If
    // the driver changes the order of the mappings then this function
    // will return an error
    match dma_descriptors_name.as_ref() {
        "dma_descriptors" => (),
        _ => return Err(MemSetupError::InvalidMemMap),
    }

    // Set up the path to the file specifying the size of the DMA
    // descriptors memory space
    let dma_descriptors_size_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DESCRIPTORS_PATH),
        Path::new(SIZE_PATH),
    ]
    .iter()
    .collect();
    // Read the size of the DMA descriptors memory space
    let dma_descriptors_size = fs::read_to_string(dma_descriptors_size_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_descriptors_size then convert to an integer.
    let dma_descriptors_size =
        usize::from_str_radix(dma_descriptors_size.trim_start_matches("0x").trim(), 16)?;

    // Set up the path to the file specifying the physical address of the
    // DMA descriptors memory space
    let dma_descriptors_phys_addr_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DESCRIPTORS_PATH),
        Path::new(ADDR_PATH),
    ]
    .iter()
    .collect();
    // Read the physical address of the DMA descriptors memory space. This
    // is set up by the kernel and written to a file so we can access it.
    let dma_descriptors_phys_addr = fs::read_to_string(dma_descriptors_phys_addr_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_descriptors_phys_addr then convert to an integer.
    let dma_descriptors_phys_addr = usize::from_str_radix(
        dma_descriptors_phys_addr.trim_start_matches("0x").trim(),
        16,
    )?;

    // Memory map the descriptors memory space
    let dma_descriptors_mem = unsafe {
        MmapOptions::new()
            .offset((2 * page_size::get()) as u64)
            .len(dma_descriptors_size)
            .map_mut(&*file)?
    };

    Ok((dma_descriptors_mem, dma_descriptors_phys_addr))
}
