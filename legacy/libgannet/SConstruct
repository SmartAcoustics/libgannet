env = Environment(
    CC='gcc',
    CCFLAGS=['-std=c11', '-Wall', '-Wno-unused-function'])


debug_cflags = ['-g', '-Og']
debug_linkflags = ['-Og']

variants = {
    'release': {'CCFLAGS': ['-Werror']},
    'debug': {'CCFLAGS': debug_cflags, 'LINKFLAGS': debug_linkflags}}

import os

build_nodes = {}
for variant in variants:
    build_env = env.Clone()
    build_env.AppendUnique(**variants[variant])
    build_node = SConscript(
        os.path.join('src', 'SConscript'), exports=['build_env'],
        variant_dir=variant)

    build_nodes[variant] = build_node

# Build the test wrappers
test_variant = 'release'
test_build_env = env.Clone()
test_build_env.AppendUnique(
    LIBPATH='#/'+test_variant, LIBS='gannet',
    RPATH=os.path.join(Dir('#').abspath, test_variant),
    CPPPATH='#/'+test_variant)

test_node = SConscript('tests/SConscript', exports=['test_build_env'])
Depends(test_node, build_nodes[test_variant])

# Valgrind
valgrind_envs = {}
for variant in variants:
    valgrind_env = env.Clone()
    valgrind_env.AppendUnique(
        LIBPATH='#/'+variant, LIBS='gannet',
        RPATH=os.path.join(Dir('#').abspath, variant),
        CPPPATH='#/'+variant)

    valgrind_envs[variant] = valgrind_env

SConscript('valgrind/SConscript', exports=['valgrind_envs'])
