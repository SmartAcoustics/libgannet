// SPDX-License-Identifier: GPL-2.0+

/dts-v1/;
/include/ "zynq-7000.dtsi"

/ {
	model = "Avnet picoZed";
	compatible = "avnet,picozed", "xlnx,zynq-7000";

	aliases {
		ethernet0 = &gem0;
		serial0 = &uart1;
	};

	memory@0 {
		device_type = "memory";
		reg = <0x0 0x40000000>;
	};

	chosen {
		bootargs = "earlyprintk";
		stdout-path = "serial0:115200n8";
	};

	usb_phy0: phy0 {
		compatible = "usb-nop-xceiv";
		#phy-cells = <0>;
		reset-gpios = <&gpio0 7 1>; /* MIO 7, GPIO_ACTIVE_LOW */
	};

};

&gem0 {
	status = "okay";
	phy-mode = "rgmii-id";
	phy-handle = <&ethernet_phy0>;

	ethernet_phy0: ethernet-phy@0 {
		reg = <0>;
	};
};

&sdhci0 {
	status = "okay";
};

/* Current hardware design uses MIO pins 12 and 13 for the UART. These pins
are also required by the eMMC. The sharing of the pins is performed by a
switching chip so it is possible to switch between the UART and the eMMC. If
at any point we wish to enable the eMMC we need to set the switch to eMMC,
enable SD1 in vivado, export a new ps7_init and enable it in this device
tree.*/
/*&sdhci1 {
	status = "okay";*/
	/* SD1 is connected to a non-removable eMMC flash device */
/*	non-removable;
};*/

&uart1 {
	status = "okay";
};

&usb0 {
	status = "okay";
	dr_mode = "host";
	usb-phy = <&usb_phy0>;
};

&qspi {
	status = "okay";
	primary_flash: ps7-qspi@0 {
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "micron,n25q128a13", "spansion,s25fl128s", "jedec,spi-nor";
		reg = <0x0>;
		spi-max-frequency = <50000000>;
		/* Example 16M partition table using U-Boot + U-Boot SPL */
		partition@0 {
			label = "boot";
			reg = <0x0 0xe0000>;
		};
		partition@e0000 {
			label = "ubootenv";
			reg = <0xe0000 0x20000>;
		};
		partition@100000 {
			label = "uboot";
			reg = <0x100000 0x100000>;
		};
		partition@200000 {
			label = "kernel";
			reg = <0x200000 0x4f0000>;
		};
		partition@6f0000 {
			label = "devicetree";
			reg = <0x6f0000 0x10000>;
		};
		partition@700000 {
			label = "rootfs";
			reg = <0x700000 0x400000>;
		};
		partition@b00000 {
			label = "spare";
			reg = <0xb00000 0x500000>;
		};
	};
};

