This README file contains information on the contents of the loopback PL 
reference implementation, targetting both the Zedboard and the Picozed with
FMC board. This PL is necessary for the tests accompanying libgannet.

In addition, a much reduced and simpler PL is implemented with the minimal
suffix. This implements only the direct DMA system and is much easier to
understand.

Building
========

Before opening a project and within this directory, run in the Vivado Tcl
console `source setup_picozed_loopback.tcl`. Currently the scripts are 
for Vivado 2019.2. If you want to use a different version, you can either
try using 2019.2 to load initially then upgrade to the correct version,
or try your luck with simply removing the check inside `picozed_loopback.tcl`
or `zedboard_loopback.tcl`. There's nothing contentious in the design, so there
is a good chance it will work easily.

Vivado might complain about `PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY` having a 
negative value. This seems to be an interaction between the FMC design and
a slightly over-zealous warning by vivado and can be safely ignored. See 
[this forum post](https://forums.xilinx.com/t5/Design-Entry/2017-2-PSU-1-critical-warning-with-basic-Zynq-design-on-DDR/td-p/789339) for more information.

At this point, you should be able to generate the bitstream, which has been
configured to output a stripped `.bin` as well as the `.bit` file.

You should also be able to export the relevant board-support files from 
File->Export->Export Hardware. The XSA file is intended to be used by 
downstream Xilinx tools but is really a `.zip` file in disguise. Change the 
name of the xsa file to end in `.zip` and then extract the files. The 
important files are `ps7_init_gpl.c` and `ps7_init_gpl.h` (AFAICT these are
identical to the none-`_gpl` variants). Those files set up the FPGA properly
for the board configuration and are different to the default Picozed files, so
in order to run the example code you'll need to use ones you have generated.

Actually building the linux system using those files is beyond the scope of
this document, but is a necessary step in getting the DMA working.

Place the resultant bitstream in the relevant location where the boot system
can pick it up.

Patches
=======

Please submit any patches against libgannet to the maintainer:

Maintainer: Toby Gomersall <toby.gomersall@smartacoustics.co.uk>
